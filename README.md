# L0MDT Output to Felix

This repo contains an alternative L0MDT DAQ implementation intended to
provide an approximate resource estimate.  It is limited in scope to
readout of one hit pipeline to one FELIX output fiber per instance
of the entity below.  Each entity is intended to run independently,
with it's own trigger FIFO.

There is no "event building", just formatting,
wherein a header and trailer are attached to each event fragment.
Error checking and monitoring features are currently minimal.

Working on interim top-level with TTC and hit simulators for
Artix-7 board testing.

Status as of tag **v0.2.3**:

  * All hits now output, but last word has rubbish in upper hit slots if number of hits is not 4
  * Valid hit count in last word stuck in trailer as interim fix

Status as of tag **v0.2.2**:

  * Only "full" output data words with 4 hits sent ("orphaned" hits never output)

Status as of tag **v0.2.0**:  

  * functional simulation working with some bugs
  * one pipeline-fiber block implements in VU3P at 320MHz
  * resource use is total ~ 2k LUTS, 2.2k FFs and 33 BRAM tiles (this
    is for 32 window matchers)
	
Here are the top-level ports for one pipeline/fiber block:

``` VHDL
    port (
    -- timing
    sys_clk    : in  std_logic;         -- pipeline clock
    sys_rst    : in  std_logic;         -- active high asynch reset
    sys_bx_stb : in  std_logic;         -- BX strobe
    ttc_bc0    : in  std_logic;         -- BC0 (not used?)
    -- trigger
    ttc_l1a    : in  std_logic;         -- Trigger strobe
    ttc_evn    : in  std_logic_vector(11 downto 0);  -- event number 12 bits only
    ttc_bcid   : in  std_logic_vector(11 downto 0);  -- trigger BCID
    ttc_orid   : in  std_logic_vector(11 downto 0);  -- trigger Orbit
    -- MDT hit data in, 320MHz
    hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);  -- MDT hit data
    hit_dv     : in  std_logic;         -- MDT hit data valid
    -- FELIX data out, 40MHz, synch'd to bx_stb
    felix      : out std_logic_vector(229 downto 0);  -- FELIX output data
    felix_dv   : out std_logic;         -- FELIX data valid
    felix_full : in  std_logic);        -- FELIX FIFO full
```

The `sys_` items are assumed to be global timing signals (320MHz
clock, active high reset, BX strobe).  The `ttc_` signals are sampled
only when `sys_bx_stb='1'` and should contain global tags for a
trigger.

`hit` is the MDT input data.  In the current implementation the width
must be less than about 44 bits as it is packed into a 48-bit field in
the output data with 4 bits of additional info.  `hit_dv` is the hit
data valid and may be active on any number of consecutive clock
cycles.

Output is formatted as described below on `felix` as a 230-bit word.
`felix_dv` is synchronized to `sys_bx_stb`.  The `felix_full` input is
currently ignored but should be very easy to implement as in input to
stop the state machine in `format.vhd`.

This module contains logic to read MDT hit data from a L0MDT DAQ
stream as event fragments and pack into a FELIX output stream.  Many
simplifying assumptions are made:

## Output Format

The output format below is very preliminary.

Input data is ~ 42 bits wide (above) and output width is 230 bits.
4 hits are output in one output word (in principle 5 would fit).
Many things are missing such as error flags, CRC in the trailer, 
board and fiber ID etc.  These are easily added.

### Header Word

| Field     | Type | Spare      | EvN    | OrN    | BcN    |
|-----------|------|------------|--------|--------|--------|
| Size:     | 4    | 190        | 12     | 12     | 12     |
| Contents: | X"4" | -reserved- | Event# | Orbit# | Bunch# |

### Data Word

Note that each MDT hit already contains data valid along with CSMID and TDCID.

| Field     | Type | Spare      | LastHit  | Hit3    | Hit2    | Hit1    | Hit0    |
|-----------|------|------------|----------|---------|---------|---------|---------|
| Size:     | 4    | 30         | 4        | 48      | 48      | 48      | 48      |
| Contents: | X"8" | -reserved- | hit 0..3 | MDT HIT | MDT HIT | MDT HIT | MDT HIT |

### Trailer Word

| Field     | Type | Spare      | EvN    | OrN    | BcN    |
|-----------|------|------------|--------|--------|--------|
| Size:     | 4    | 190        | 12     | 12     | 12     |
| Contents: | X"C" | -reserved- | Event# | Orbit# | Bunch# |

### Idle Word

When there is no data to send, and idle word is sent.

| Field    | Type | Spare    |
|----------|------|----------|
| Size:    | 4    | 226      |
| Contents | X"0" | Reserved |

