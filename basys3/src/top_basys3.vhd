-- top_basys3.vhd
--
-- 7-series test of DAQ for hardware demo
-- random trigger generator
-- random hit generator
--
-- for now run at 100MHz so about 1/3 of the LHC rate
-- BX rate is 12.5MHz
--
-- so L1A at LHC 1MHz -> 312.5kHz
-- tube hits at 200kHz * 24 * 18 = 86.4MHz LHC -> 27MHz
--

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_basys3 is

  port (
    sys_clk : in  std_logic;
    sw      : in  std_logic_vector(15 downto 0);
    btnC    : in  std_logic;
    led     : out std_logic_vector(15 downto 0);
    JA      : out std_logic_vector(7 downto 0)
    );

end entity top_basys3;

architecture arch of top_basys3 is

  signal count      : unsigned(31 downto 0);
  signal sys_rst    : std_logic;
  signal sys_bx_stb : std_logic;
  signal trig       : std_logic;
  signal hit        : std_logic;

  signal trg_rate : std_logic_vector(31 downto 0);
  signal hit_rate : std_logic_vector(31 downto 0);

begin

  sys_rst <= btnC;

  process (sys_clk, sys_rst) is
  begin  -- process
    if sys_rst = '1' then               -- asynchronous reset (active high)
      count <= (others => '0');
    elsif rising_edge(sys_clk) then     -- rising clock edge
      count <= count + 1;

      if count(2 downto 0) = "000" then  -- BX every 8 clocks
        sys_bx_stb <= '1';

        -- switches 0, 1 select 1kHz, 10kHz, 100kHz, 1MHz LHC-equivalent triggers
        -- the bx_stb rate is 12.5 MHz here
        with sw(1 downto 0) select
          trg_rate <= x"3fff_ffff" when "11",  -- "1MHz:" (2e31-1)/4 (312.5kHz)
          x"020c_49ba"             when "10",  -- 100kHz: /40
          x"0034_6dc5"             when "01",  -- 10kHz: /400
          x"0005_3e2d"             when "00";  -- 1kHz: /5000

        -- switches 2, 3 select 10MHz, 20MHz, 40MHz, 80MHz simulated LHC-equ triggers
        -- the clk rate here is 100MHz
        with sw(3 downto 2) select
          hit_rate <= x"7fff_ffff" when "11",  -- 10 MHz: (3.125M): (2e31-1)/32
          x"0fff_ffff"             when "10",  -- 20 MHz: (6.25M)  /16
          x"1fff_ffff"             when "01",  -- 40 MHz: (12.5M)  /8
          x"3fff_ffff"             when "00";  -- 80 MHz: (25M)    /4

      else
        sys_bx_stb <= '0';
      end if;
    end if;
  end process;

  JA(0) <= sys_bx_stb;
  JA(1) <= trig;
  JA(2) <= sys_clk;
  JA(3) <= hit;

  JA(7 downto 4) <= (others => '0');

  led <= trg_rate(15 downto 0);

  trig_gen_1 : entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      rate       => trg_rate,
      trig       => trig);

  trig_gen_2 : entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => '1',
      rate       => hit_rate,
      trig       => hit);

end architecture arch;
