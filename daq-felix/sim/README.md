# Eric's DAQ - Functional Simulation

This directory contains tools to evaluate the performance of
the VHDL DAQ code for the MDTTP.  The source code is expected
to be in `../sim`.

This is all very much a work in progress.

## `Makefile`

Builds the tools.  Useful targets for now:

`all`

Build the `make_wm_tb` C program and compile all VHDL using `ghdl`.

`run`

Run the `ghdl` simulation, producing `daq_tb.ghw`.  Simulation time is specified in 
`Makefile` as `DAQTIM`.

`view`

Display waveforms.  Uses `daq_tb.tcl` to specify waveforms to view.

## `daq_tb.vhd`

The main testbench for the top-level `daq`.  Reads inputs from `input_vectors.txt` and
writes results to `output_results.txt`.  Input format:

```
T 0 57 0 57 2
H 0 62 0 62 1
```

`T` is for trigger, `H` is for MDT hit.  Numbers are: (OrN, BcN, Phase) where
the vector should be activated, followed by the effective BcN for the trigger or hit and
finally a serial number which can be used to track the data through the simulation.

Output format is raw hex FELIX data, one 230-bit word per line.

## `make_wm_tb.c`

C program to generate random test data.  Brief usage:

```
  ./make_wm_tb [-r seed] [-o orbits] [-h hit_rate_hz]
               { [-t trig_rate_hz] | [-s uniform_trig_spacing_bx]}
```

* `seed` is random number seed (default: 0)
* `orbits` is number of orbits to generate (default: 1)
* `hit_rate_hz` is MDT hit rate in Hz (default: 80e6)
* `trig_rate_hz` is trigger rate in Hz (default: 1e6)
* `uniform_trig_spacing_bx` is alternative fixed trigger spacing in BX

## `grok_results.pl`

Read `output_results.txt` (raw hex) and format in a human-readable form, like so:

```
HEAD: 000 014 001
 HIT: ID: 00000001 BCN: 03E PHA 0
 HIT: ID: 00000002 BCN: 03F PHA 1
 HIT: ID: 00000003 BCN: 03F PHA 6
  ...
TAIL: 000 014 001
```

`HEAD` and `TAIL` are the header and trailer.  `HIT` are MDT hit data.

## `check_daq.pl`

Perl script to read `input_vectors.txt` and simulate the DAQ,
producing output same as `grok_results.pl` above.  It isn't perfect;
there are quite a few edge cases at the beginning and end of trigger
windows which are handled slightly differently from the HDL logic.

Output to `stdout` is for debugging; file `sim_daq_output.txt` contains
the format mentioned below.

## `compare_daq_sim.pl`

Compare perl and ghdl simulation.  This script invokes `./grok_results.pl output_results.txt` and
`check_daq.pl input_vectors.txt` in pipelines and compares the results.

# Running it all

You have to create an input file 'input_vectors.txt' using
`make_wm_tb`.  The defaults are fine:  

`make_wm_tb > input_vectors.txt`

Then, `make run` and finally `./compare_daq_sim.pl`.

The simulation should run for approximately one orbit (e.g. 100us).
This results in a fairly good match as reported by
`compare_daq_sim.pl`.



