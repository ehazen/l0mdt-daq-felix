#!/usr/bin/perl
#
# Simulate Eric's L0MDT DAQ
#
# Expects to read generic parameters from "daq_tb.vhd" to get the
# DAQ configuration:
#
# -- configuration of window match entities
#    constant NUM_WM          : integer := 4;
#    constant TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
#    constant TRIG_WIN_WIDTH  : integer := 40;  -- width of trigger window in BX
#    constant TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
#    constant TRIG_TIMEOUT    : integer := 40;  -- max additional time to wait for FIFO to empty
# -- end of configuration from VHDL
#
# Read input vectors from file(s) on command line in the format:
#
# T/H 0 63 4 68 1
#
#   T/H is "T" for trigger, "H" for hit
#   (0, 63, 4) is (OrN, BcN, pha) when vector is asserted (pha is clock phase 0..7)
#   68 is the BcN contained in the Hit or Trigger
#
# outputs to stdout some human-readable results
# (verbosity level set in code with $verbose)
#
# outputs to file "sim_daq_output.txt" hex format like:
#
#  HEAD: 000 12C 001
#   HIT: ID: 00000039 BCN: 13D PHA 3
#   HIT: ID: 0000003A BCN: 140 PHA 1
#   HIT: ID: 0000003B BCN: 145 PHA 4
#   HIT: ID: 0000003C BCN: 146 PHA 3
#  TAIL: 000 12C 001
#
# Details:
# Array of window matchers is kept in @wma (array of hash references)
#  Each WM has members:
#    STATE:     IDLE (not busy), MATCH (waiting for hits), WAIT (waiting for readout)
#    W_BEGIN:   start of match window in BX
#    W_END:     end of match window in BX
#    M_EXPIRE:  BcN when matching ends
#    TIMEOUT:   BcN when WM becomes IDLE
#    FIFO:      hit FIFO (reference to array)
#
# As records are read from input file, WM states are updated,
# triggers are assigned to WM and hits are matched and stored
# in WM FIFO.  As each WM ends it's WAIT state, hits are read
# from the FIFO and output.  This isn't exactly how the HDL
# works but should produce the same results
#

use strict;
use Data::Dumper;

# trigger parameters from testbench

open my $ftb, "<", "daq_tb.vhd" or die "Can't find daq_tb.vhd";

my %par;
my $verbose = 0;

while( my $line = <$ftb>) {
    chomp $line;
    if( $line =~ /^\s+constant\s/) {
	my ($nam, $typ, $val) = $line =~ /^\s+constant\s+(\w+)\s+:\s+(\w+)\s+:=\s+([^;]+);/;
	if (uc $typ eq "INTEGER") {
	    $par{$nam} = $val;
	}
    }
}


# extract some useful values
sub getpar {
    my ($pn, $def) = @_;
    my $val = $def;
    if( $par{$pn}) {
	$val = $par{$pn};
    } else {
	print "Warning:  missing parameter value for $pn\n";
    }
    return $val;
}

my $num_wm = getpar( "NUM_WM", -1);
my $mat_tim = getpar( "TRIG_MATCH_TIME", -1);
my $timeout = getpar( "TRIG_TIMEOUT", -1);
my $win_off = getpar( "TRIG_WIN_OFFSET", -1);
my $win_wid = getpar( "TRIG_WIN_WIDTH", -1);

my @wma;			# array of window matchers
my $nwm = 0;			# next window matcher

for( my $i=0; $i<$num_wm; $i++) {
    $wma[$i] = { STATE => "IDLE"};
}

#--- now read the vectors and match hits to triggers ---
while( my $line = <>) {
    chomp $line;
    next if( $line =~ /^#/);
    my @f = split ' ', $line;
    my $nf = scalar @f;
    die "back input in ($line)" if( $nf != 6);

    my $sim_bcn = $f[2];
    my $hit_bcn = $f[4];
    my $seq = $f[5];

    if( $f[0] eq "T") {
	print "# Trigger $seq at $hit_bcn next WM=$nwm\n" if($verbose);
	my $wm = $wma[$nwm];
	
	# configure a window matcher
	if( $wm->{STATE} eq "IDLE") {
	    $wm->{STATE} = "MATCH";                     # state to MATCH
	    $wm->{TRIG} = $line;			# save trigger record
	    $wm->{W_BEGIN} = $hit_bcn + $win_off;	# start of match window
	    $wm->{W_END} = $wm->{W_BEGIN} + $win_wid;	# end of match window
	    $wm->{M_EXPIRE} = $sim_bcn + $mat_tim;	# expiration BCN of match window
	    $wm->{TIMEOUT} = $wm->{M_EXPIRE} + $timeout; # timeout BCN
	    $wm->{FIFO} = [ ];				 # empty FIFO
	    print Dumper( $wm) if( $verbose > 1);
	    $nwm = ($nwm + 1) % $num_wm;
	} else {
	    print "# All window matchers busy\n";
	    for( my $i=0; $i<$num_wm; $i++) {
		my $wm = $wma[$i];
		print "# DUMP wm $i\n";
		print Dumper($wm);
	    }
	    exit;
	}
    }

    if( $f[0] eq "H") {
	# see if this hit matches any windows
	for( my $i=0; $i<$num_wm; $i++) {
	    my $wm = $wma[$i];
	    if( $wm->{STATE} eq "MATCH" && $hit_bcn >= $wm->{W_BEGIN} &&
		$hit_bcn <= $wm->{W_END}) {
		print "# HIT matches wm $i: $line\n" if( $verbose > 1);
		push @{$wm->{FIFO}}, $line;
	    }
	}

    }

    # check WM status changes
    for( my $i=0; $i<$num_wm; $i++) {
	my $wm = $wma[$i];

	if( $verbose > 4 && $wm->{STATE} ne "IDLE") {
	    print "# Check WM $i at $sim_bcn... state=", $wm->{STATE}, "\n";
	}

	if( $wm->{STATE} eq "MATCH") {
	    if( $sim_bcn >= $wm->{M_EXPIRE}) {
		print "# WM $i matching ended at $sim_bcn\n" if($verbose);
		$wm->{STATE} = "WAIT";
	    }
	}

	if( $wm->{STATE} eq "WAIT") {
	    if( $sim_bcn >= $wm->{TIMEOUT}) {
		print "# WM $i timed out at $sim_bcn\n" if($verbose);
		$wm->{STATE} = "IDLE";
		# empty it's fifo
		print "# EVENT: ", $wm->{TRIG}, "\n" if( $verbose);
		my @d = split ' ', $wm->{TRIG};
		my $hdr = sprintf "HEAD: 000 %03X 001\n", $d[4];
		my $trl = sprintf "TAIL: 000 %03X 001\n", $d[4];
		print $hdr;
		while( scalar @{$wm->{FIFO}}) {
		    my $h = shift @{$wm->{FIFO}};
		    print "# HIT: $h\n" if( $verbose);
		    my @hd = split ' ', $h;
		    my $hit = sprintf " HIT: ID: %08X BCN: %03X PHA %d\n", $hd[5], $hd[4], $hd[3];
		    print $hit;
		}
		print $trl;
	    }
	}
    }
    
}
