//
// generate NR randoms
//

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#define K1 1664525
#define K2 1013904223

int main( int argc, char *argv[]) {

  uint32_t seed, u;

  if( argc > 1)
    seed = strtoul( argv[1], NULL, 0);
  else
    seed = 0;

  u = seed;

  while(1) {
    u = (u*K1) + K2;
    printf("0x%08x\n", u);
  }

}
