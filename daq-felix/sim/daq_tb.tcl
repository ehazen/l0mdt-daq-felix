#
# default top-level display for daq_tb
#
gtkwave::addSignalsFromList [list {top.daq_tb.sys_clk}]
gtkwave::addSignalsFromList [list {top.daq_tb.sys_rst}]
gtkwave::addSignalsFromList [list {top.daq_tb.sys_bx_stb}]
gtkwave::addSignalsFromList [list {top.daq_tb.ttc_bc0}]
gtkwave::addSignalsFromList [list {top.daq_tb.ttc_l1a}]
gtkwave::addSignalsFromList [list {top.daq_tb.ttc_bcid}]
gtkwave::addSignalsFromList [list {top.daq_tb.ttc_orid}]

gtkwave::addSignalsFromList [list {top.daq_tb.s_hit_dv}]
gtkwave::addSignalsFromList [list {top.daq_tb.s_hit}]

gtkwave::addSignalsFromList [list {top.daq_tb.felix_dv}]
gtkwave::addSignalsFromList [list {top.daq_tb.felix}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[0].wm_1.state}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[0].wm_1.empty}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[0].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[0].wm_1.fifo_re}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[1].wm_1.state}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[1].wm_1.empty}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[1].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[1].wm_1.fifo_re}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[2].wm_1.state}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[2].wm_1.empty}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[2].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[2].wm_1.fifo_re}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[3].wm_1.state}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[3].wm_1.empty}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[3].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.dispatch_1.wms[3].wm_1.fifo_re}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.format_1.felix_header}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.format_1.felix_data}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.format_1.felix_trailer}]
gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.format_1.data_out}]

gtkwave::addSignalsFromList [list {top.daq_tb.daq_1.format_1.state}]


gtkwave::/Time/Zoom/Zoom_Full
