--
-- daq_unit_tb.vhd - testbench for daq_unit using GHDL
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity daq_unit_tb is
end entity daq_unit_tb;

architecture sim of daq_unit_tb is

  -- things we might want to change...
  constant TRIG_WIN_OFFSET   : integer := 10;  -- offset to start of trigger match (BX)
  constant TRIG_WIN_WIDTH    : integer := 40;  -- width of trigger window in BX
  constant TRIG_MATCH_TIME   : integer := 80;  -- min time (BX) to wait for matching hits
  constant TRIG_TIMEOUT      : integer := 200;  -- max time to wait for FIFO to empty
  --
  constant NUM_WM            : integer := 4;  -- No. window match.  MUST MATCH NEXT GENERIC
  constant WM_SEL_WID        : integer := 2;  -- select width for above MUST MATCH

  component daq_unit is
    generic (
      ORBIT_LENGTH      : integer;
      PIPE_CLK_MULTIPLE : integer;
      HIT_WIDTH         : integer;
      BX_BIT_OFFSET     : integer;
      TRIG_WIN_OFFSET   : integer;
      TRIG_WIN_WIDTH    : integer;
      TRIG_MATCH_TIME   : integer;
      TRIG_TIMEOUT      : integer;
      DAQ_WIDTH         : integer;
      DAQ_MULT          : integer;
      SLOT_WIDTH        : integer;
      NUM_WM            : integer;
      WM_SEL_WID        : integer);
    port (
      sys_clk    : in  std_logic;
      sys_rst    : in  std_logic;
      trig_rate  : in  std_logic_vector(31 downto 0);
      hit_rate   : in  std_logic_vector(31 downto 0);
      ocr_req    : in  std_logic;
      ecr_req    : in  std_logic;
      felix      : out std_logic_vector(229 downto 0);
      felix_dv   : out std_logic;
      felix_full : in  std_logic;
      trig_valid : out std_logic;
      hit_dv     : out std_logic);
  end component daq_unit;

begin


  g_clk : process
  begin
    while not stop_the_clock loop
      s_clk    <= '1', '0' after clock_period / 2;
      sim_tick <= sim_tick + 1;
      if sim_pha = 7 then
        sim_pha <= 0;
      else
        sim_pha <= sim_pha + 1;
      end if;
      wait for clock_period;
    end loop;
  end process;

  daq_unit_1: entity work.daq_unit
    generic map (
      TRIG_WIN_OFFSET   => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH    => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME   => TRIG_MATCH_TIME,
      TRIG_TIMEOUT      => TRIG_TIMEOUT,
      NUM_WM            => NUM_WM,
      WM_SEL_WID        => WM_SEL_WID)
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      trig_rate  => trig_rate,
      hit_rate   => hit_rate,
      ocr_req    => ocr_req,
      ecr_req    => ecr_req,
      felix      => felix,
      felix_dv   => felix_dv,
      felix_full => felix_full,
      trig_valid => trig_valid,
      hit_dv     => hit_dv);


end architecture sim;

