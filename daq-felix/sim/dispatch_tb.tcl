#
# default top-level display for dispatch_tb
#
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_clk}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_bx_stb}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_bc0}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_trigger}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_hit_dv}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.next_wm}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_busy_flags}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.s_all_busy}]

gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[0].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[1].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[2].wm_1.fifo_we}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[3].wm_1.fifo_we}]

gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[0].wm_1.fifo_empty}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[1].wm_1.fifo_empty}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[2].wm_1.fifo_empty}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[3].wm_1.fifo_empty}]

gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[0].wm_1.state}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[1].wm_1.state}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[2].wm_1.state}]
gtkwave::addSignalsFromList [list {top.dispatch_tb.dispatch_1.wms[3].wm_1.state}]

gtkwave::/Time/Zoom/Zoom_Full
