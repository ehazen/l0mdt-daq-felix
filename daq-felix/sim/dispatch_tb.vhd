--
-- simple testbench for dispatcher / cluster of window managers
--
-- reads triggers and hits from a file "input_vectors.txt"
--
-- input vector format
--   flag s_orn s_bcn s_pha e_bcn
--
--   flag is "T" or "H" for trigger or hit
--   next 3 are timestamp to apply vector
--     s_orn - orbit number, starting with 0
--     s_bcn - bunch crossing, 0-3563
--     s_pha - pipeline clock phase, 0-7
--   e_bcn is the BcN of the "event" (trigger or hit)
--
-- no readout
--
-- E.Hazen
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;


entity dispatch_tb is
end entity dispatch_tb;

architecture sim of dispatch_tb is

-- configuration of window match entities
  constant NUM_WM          : integer := 4;
  constant WM_SEL_WID      : integer := 2;
  constant HIT_WIDTH       : integer := 41;  -- input HIT data width in bits
  constant BX_BIT_OFFSET   : integer := 0;   -- offset of BX in input hits
  constant TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
  constant TRIG_WIN_WIDTH  : integer := 40;  -- width of trigger window in BX
  constant TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
  constant TRIG_TIMEOUT    : integer := 40;  -- max additional time to wait for FIFO to empty

  constant clock_period : time := 3.125 ns;
  signal stop_the_clock : boolean;      -- needed to stop XIlinx simulator

  -- signals to wire up DUT
  signal s_clk      : std_logic;
  signal s_rst      : std_logic;
  signal s_bx_stb   : std_logic;
  signal s_bc0      : std_logic;
  signal s_trigger  : std_logic;
  signal s_bcid     : std_logic_vector(11 downto 0);
  signal s_orid     : std_logic_vector(11 downto 0);
  signal s_all_busy : std_logic;
  signal s_matching : std_logic;
  signal s_busy     : std_logic;
  signal s_hit      : std_logic_vector(HIT_WIDTH-1 downto 0);
  signal s_hit_dv   : std_logic;
  signal s_wm_sel   : std_logic_vector(WM_SEL_WID-1 downto 0);
  signal s_empty    : std_logic;
  signal s_full     : std_logic;
  signal s_re       : std_logic;
  signal s_data     : std_logic_vector(HIT_WIDTH-1 downto 0);


  file file_VECTORS : text;
  file file_RESULTS : text;

  -- these are shared so they can be updated in a clock/timing process
  -- they are only read elsewhere
  shared variable sim_bcn : integer;
  shared variable sim_orn : integer;
  shared variable sim_pha : integer;

begin  -- architecture sim

  -- instantiate the DUT
  dispatch_1 : entity work.dispatch
    generic map (
      NUM_WM          => NUM_WM,
      WM_SEL_WID      => WM_SEL_WID,
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT)
    port map (
      sys_clk      => s_clk,
      sys_rst      => s_rst,
      sys_bx_stb   => s_bx_stb,
      ttc_bc0      => s_bc0,
      ttc_l1a  => s_trigger,
      ttc_bcid     => s_bcid,
      ttc_orid     => s_orid,
      wm_busy     => s_busy,
      all_busy => s_all_busy,
      hit      => s_hit,
      hit_dv   => s_hit_dv,
      wm_sel   => s_wm_sel,
      wm_matching => s_matching,
      wm_empty    => s_empty,
      wm_full     => s_full,
      wm_re       => s_re,
      wm_data     => s_data);

  stimulus : process is

    -- variables used for file I/O
    variable row                        : line;
    variable buff                       : line;
    variable flag                       : character;
    variable s_bcn, s_orn, e_bcn, s_pha : integer;
    variable fst                        : file_open_status;

  begin

    file_open(fst, file_VECTORS, "input_vectors.txt", read_mode);
    file_open(fst, file_RESULTS, "output_results.txt", write_mode);

    write(buff, string'("files open"));
    writeline(output, buff);

    -- Put initialisation code here
    s_trigger <= '0';
    s_bcid    <= (others => '0');
    s_orid    <= (others => '0');
    s_hit     <= (others => '0');
    s_hit_dv  <= '0';
    s_re      <= '0';

    sim_bcn := 0;
    sim_orn := 0;
    sim_pha := 0;

    s_rst <= '1';
    wait for clock_period*4;
    s_rst <= '0';
    wait for clock_period*4;

    wait for clock_period*24;

    write(buff, string'("loop reading"));
    writeline(output, buff);

    -- read the first vector so it's ready
    while true loop
      readline(file_VECTORS, row);
      read(row, flag);

      if flag /= '#' then               --ignore comments
        read(row, s_orn);
        read(row, s_bcn);
        read(row, s_pha);
        read(row, e_bcn);
        exit;
      end if;

    end loop;

--    -- debug output
--    write(buff, string'("First VEC (orn, bcn, pha) "));
--    write(buff, s_orn);
--    write(buff, ' ');
--    write(buff, s_bcn);
--    write(buff, ' ');
--    write(buff, s_pha);
--    writeline(output, buff);

    -- this is an arbitrary offset to avoid issues with setup/hold
    -- times not being met due to floating rounding errors
    wait for clock_period/3;

    --------------------------------------------------
    -- main loop... stop when vector file is empty
    -- (or killed by simulation time limit)
    --------------------------------------------------

    while not endfile(file_VECTORS) loop

      wait for clock_period;            -- advance simulation one tick

      -- is the next vector ready to be asserted?
      if (sim_orn = s_orn) and (sim_bcn = s_bcn) and (sim_pha = s_pha) then

        -- yes, check for (T)rigger or (H)it record (ignore others)
        if flag = 'T' then
          -- process trigger
          s_bcid    <= std_logic_vector(to_unsigned(e_bcn, s_bcid'length));
          s_trigger <= '1';
          s_trigger <= transport '0' after clock_period*8;
          write(buff, string'("Trigger at "));
          write(buff, now);
          writeline(output, buff);
        elsif flag = 'H' then
          -- process hit
          s_hit_dv                                     <= '1';
          s_hit_dv                                     <= transport '0' after clock_period;
          s_hit(BX_BIT_OFFSET+11 downto BX_BIT_OFFSET) <= std_logic_vector(to_unsigned(e_bcn, 12));
          -- put some extra data in the hit for simulation
          s_hit(15 downto 12)                          <= std_logic_vector(to_unsigned(s_pha, 4));
          -- FIXME: should put some sort of unique ID in upper bits for validation
          s_hit(HIT_WIDTH-1 downto 16)                 <= (others => '0');
          write(buff, string'("hit at "));
          write(buff, now);
          writeline(output, buff);
        end if;

        -- read next vector
        readline(file_VECTORS, row);
        read(row, flag);
        if flag /= '#' then
          read(row, s_orn);
          read(row, s_bcn);
          read(row, s_pha);
          read(row, e_bcn);
        end if;

      end if;

    end loop;
    --------------------------------------------------
    -- end of main loop
    --------------------------------------------------

    wait;

  end process;

  --------------------------------------------------
  -- generate clock and basic TTC timing
  -- update sim time:  sim_orn, sim_bcn, sim_pha
  --------------------------------------------------

  g_clk : process                       -- generate 320MHz clock
  begin
    while not stop_the_clock loop
      s_clk   <= '0', '1' after clock_period / 2;
      wait for clock_period;
      sim_pha := (sim_pha + 1) mod 8;   -- update 0..7 phase count
    end loop;
  end process;

  g_bx : process  -- generate bx strobe and sim_bcn, sim_orn
  begin
    while not stop_the_clock loop
      s_bx_stb <= '1', '0' after clock_period;
      if sim_bcn = 3563 then
        sim_bcn := 0;
        sim_orn := sim_orn + 1;
        s_bc0   <= '1', '0' after clock_period * 8;
      else
        sim_bcn := sim_bcn + 1;
        s_bc0   <= '0';
      end if;
      wait for clock_period * 8;
    end loop;
  end process;

end architecture sim;
