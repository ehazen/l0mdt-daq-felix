#!/usr/bin/perl
#
# read output from Eric's L0MDT daq and parse
use strict;

open my $ftb, "<", "daq_tb.vhd" or die "Can't find daq_tb.vhd";

my %par;
my $verbose = 0;

while( my $line = <$ftb>) {
    chomp $line;
    if( $line =~ /^\s+constant\s/) {
	my ($nam, $typ, $val) = $line =~ /^\s+constant\s+(\w+)\s+:\s+(\w+)\s+:=\s+([^;]+);/;
	if (uc $typ eq "INTEGER") {
	    $par{$nam} = $val;
	}
    }
}

if( !$par{"HIT_WIDTH"} || !$par{"WM_SEL_WID"}) {
    print "Error reading daq_tb.vhd:  can't find HIT_WIDTH and/or WM_SEL_WID\n";
    exit;
}

my $trig_wid = $par{"HIT_WIDTH"} + $par{"WM_SEL_WID"};

sub hit_fmt {
    my $hit = shift @_;
    my $bcn = substr $hit, -3;
    my $pha = substr $hit, -4, 1;
    my $idn = substr $hit, 0, length($hit)-4;
    return "ID: $idn BCN: $bcn PHA $pha";
}

while( my $line = <>) {
    chomp $line;
    if( length($line) != 58) {
	print "Error: $line\n";
	exit;
    }
    my $type = substr $line, 0, 1;
    my $ev_id = substr $line, -9;
    my $evn = substr $ev_id, 0, 3;
    my $orn = substr $ev_id, 3, 3;
    my $bcn = substr $ev_id, 6, 3;
    my $slots = substr $line, -49, 1;
    $slots = 4 if( $slots == 0);

    if( $type eq "1") {
	print "HEAD: $evn $orn $bcn\n";
    }
    if( $type eq "3") {
	print "TAIL: $evn $orn $bcn\n";
    }
    if( $type eq "2") {
	my @hits;
	for( my $i=0; $i<$slots; $i++) {
	    $hits[$i] = substr $line, -12*($i+1), 12;
	    print " ";
	    print "HIT: ", hit_fmt($hits[$i]), "\n";
	}
    }

}
