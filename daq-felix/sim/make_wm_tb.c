//
// generate triggers and random hits to test WM
//
// args:  <seed> <orbits> <hit_rate_hz> <first_trig_bx> <trig_spacing_bx>
//
// output format:
// <tag> <OrN> <BcN> <Pha> <BcN2> <serial>
//
//   <tag> is "T" for trigger or "H" for hit
//   OrN, BcN, Pha are orbit/bcn and clock phase (0..7) to launch event
//   BcN2 is real-time BcN of hit (TDC coarse time) or trigger (from TTC)
//   <serial> is an integer which increases by one for each item
//      (each tag has it's own sequence)

// LHC orbit length
#define ORBIT_LEN 3564
// BX period (for rate estimate only)
#define BX_PER 25.0e-9
// pipeline clocks per BX
#define MUX_FACT 8

// first BX to start hits
#define HIT_START 60
#define HIT_END (ORBIT_LEN-60)

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

int main( int argc, char*argv[]) {

  // arguments
  unsigned int seed;		/* random seed */
  double hit_rate;		/* random hit rate in Hz */
  double trig_rate;		/* random trigger rate in Hz */
  int first_trig;		/* first trigger at BX */
  int trig_spacing;		/* trigger spacing in BX */
  int num_orbits;		/* number of orbits to generate */

  int sim_bx, sim_orbit, sim_clk;

  int serial_trig, serial_hit;

  double hit_prob, trig_prob;

  int trig_skip_clk = 0;

  int c;

  if( argc < 2) {
    //    printf("usage: %s <seed> <orbits> <hit_rate_hz> <first_trig_bx> <trig_spacing_bx>\n", argv[0]);
    printf("usage: %s [-r seed] [-o orbits] [-h hit_rate_hz]\n", argv[0]);
    printf("          { [-t trig_rate_hz] | [-s uniform_trig_spacing_bx]}\n");
    exit(0);
  }

  // defaults
  seed = 0;
  num_orbits = 1;
  hit_rate = 80e6;
  trig_rate = 1e6;
  first_trig = 0;
  trig_spacing = 0;

  while( (c = getopt( argc, argv, "r:o:h:t:s:")) != -1)
    switch( toupper( c)) {
    case 'R':
      seed = atoi(optarg);
      break;
    case 'O':
      num_orbits = atoi(optarg);
      break;
    case 'H':
      hit_rate = atof(optarg);
      break;
    case 'T':
      trig_rate = atof(optarg);
      break;
    case 'S':
      first_trig = trig_spacing = atoi(optarg);
      break;
    default:
      abort();
    }

  sim_bx = 0;
  sim_orbit = 0;
  sim_clk = 0;

  serial_trig = serial_hit = 1;

  hit_prob = (double) hit_rate * ((double)BX_PER/MUX_FACT);
  trig_prob = (double) trig_rate * ((double)BX_PER);

  long hit_thresh = hit_prob * RAND_MAX;
  long trig_thresh = trig_prob * RAND_MAX;

  srandom( seed);

  printf("#");
  for( int i=0; i<argc; i++)
    printf(" %s", argv[i]);
  printf("\n");
  
  printf("# hit probability: %g  hit threshold %ld / %ld\n", hit_prob, hit_thresh, (long)RAND_MAX);
  printf("# trig probability: %g  trig threshold %ld / %ld\n", trig_prob, trig_thresh, (long)RAND_MAX);

  while( sim_orbit < num_orbits) {

    while( sim_bx < ORBIT_LEN) {

      trig_skip_clk = 0;

      // periodic triggers
      if( first_trig != 0) {
	if( (sim_bx >= first_trig) && ((sim_bx-first_trig) % trig_spacing) == 0) {
	  printf("T %d %d %d %d %d\n", sim_orbit, sim_bx, sim_clk, sim_bx, serial_trig);
	  ++serial_trig;
	  trig_skip_clk = 1;
	}

	// random triggers
      } else {
	if( random() < trig_thresh) {
	  printf("T %d %d %d %d %d\n", sim_orbit, sim_bx, sim_clk, sim_bx, serial_trig);
	  ++serial_trig;
	  trig_skip_clk = 1;
	}
      }

      if( sim_bx > HIT_START && sim_bx < HIT_END) {
	// multiple hits per BX OK
	for( int i=trig_skip_clk; i<MUX_FACT; i++) {
	  if( random() < hit_thresh) {
	    printf("H %d %d %d %d %d\n", sim_orbit, sim_bx, i, sim_bx, serial_hit);
	    ++serial_hit;
	  }
	}
      }


      ++sim_bx;
    }
    sim_bx = 0;
    
    ++sim_orbit;
  }

}
