--
-- test some broken simulator features
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.math_real.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity test is

  generic (
    HIT_WIDTH     : integer := 41;      -- HIT data width
    BX_BIT_OFFSET : integer := 0;       -- offset of BX in input hits
    TRIG_RATE_KHZ : real    := 1000.0;    -- trigger rate in kHz
    HIT_RATE_MHZ  : real    := 82.0);     -- tube hit rate in MHz

end entity test;

architecture sim of test is

  constant TRIG_THR : real := ((2.0**32.0)-1.0)*(TRIG_RATE_KHZ/40.0e3);
  signal urate : unsigned(31 downto 0) := to_unsigned( natural(TRIG_THR), 32);

begin

  sim : process
    variable buff : line;
  begin

    write( buff, TRIG_THR);
    writeline( output, buff);

    hwrite(buff, std_logic_vector(urate));
    writeline(output, buff);

    wait;

  end process;

end architecture sim;
