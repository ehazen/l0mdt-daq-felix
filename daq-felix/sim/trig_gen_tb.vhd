-- test the trigger generator
-- 

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;


entity trig_gen_tb is
end entity trig_gen_tb;

architecture sim of trig_gen_tb is

  constant verbose : integer := 1;

--  constant half_clock_period : time := 1.5625 ns;
  constant half_clock_period : time := 1.5 ns;
  constant clock_period      : time := 2 * half_clock_period;
  signal stop_the_clock      : boolean;  -- needed to stop XIlinx simulator

  signal sys_clk    : std_logic := '0';
  signal sys_rst    : std_logic := '0';
  signal sys_bx_stb : std_logic := '0';

  file file_VECTORS : text;
  file file_RESULTS : text;

  -- simulator time
  signal sim_tick : integer                 := 0;
  signal sim_bcn  : integer range 0 to 4095 := 0;
  signal sim_orn  : integer range 0 to 4095 := 0;
  signal sim_pha  : integer range 0 to 7    := 7;

  signal rate : std_logic_vector(31 downto 0);

  signal trig : std_logic;

begin  -- architecture sim

--  rate <= x"0666_6666";                 -- this is (2^32-1)/40 for 1MHz triggers
  rate <= x"3fff_ffff";                 -- 1/4 for 80MHz hits

  stimulus : process(sys_clk) is

    -- variables used for file I/O
    variable row                        : line;
    variable buff                       : line;
    variable outb                       : line;
    variable flag                       : character;
    variable s_bcn, s_orn, e_bcn, s_pha : integer;
    variable seq_no                     : integer;
    variable fst                        : file_open_status;

  begin

    if rising_edge(sys_clk) then        -- rising clock edge

      -- assert reset for 4 clocks
      if sim_tick < 4 then
        sys_rst <= '1';
      else
        sys_rst <= '0';
      end if;

      -- open files on tick 5
      if sim_tick = 5 then

        file_open(fst, file_RESULTS, "output_results.txt", write_mode);

        write(buff, string'("files open"));
        writeline(output, buff);

      end if;

      if sim_tick > 5 then

--        if sys_bx_stb = '1' and trig = '1' then     -- for trigger test
        if trig = '1' then                       -- for hit test
          write(buff, string'("trigger at "));
          write(buff, now);
          writeline(file_results, buff);

          write(buff, string'("trigger at "));
          write(buff, now);
          writeline(output, buff);

        end if;

      end if;


    end if;  -- rising_edge( sys_clk);
    --------------------------------------------------
    -- end of main loop
    --------------------------------------------------

  end process;

--------------------------------------------------
-- generate clock and basic TTC timing
-- update sim time:  sim_orn, sim_bcn, sim_pha
--------------------------------------------------

  -- first just generate the clock; all else runs off this clock
  g_clk : process
  begin
    while true loop
      sys_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
  end process;

  -- synchronous process to update other things
  process (sys_clk) is
  begin  -- process
    if rising_edge(sys_clk) then        -- rising clock edge

      sim_tick <= sim_tick + 1;

      if sim_pha = 7 then
        sim_pha    <= 0;
        sys_bx_stb <= '1';

        if sim_bcn = 3563 then
          sim_bcn <= 0;
        else
          sim_bcn <= sim_bcn + 1;
        end if;

      else
        sim_pha    <= sim_pha + 1;
        sys_bx_stb <= '0';
      end if;

    end if;
  end process;

  trig_gen_1 : entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
--      sys_bx_stb => sys_bx_stb,       -- for trigger test
      sys_bx_stb => '1',                -- for hit test
      rate       => rate,
      trig       => trig);

end architecture sim;
