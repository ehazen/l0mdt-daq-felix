--
-- ttc_eric testbench
--
-- E.Hazen
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;


entity ttc_eric_tb is
end entity ttc_eric_tb;

architecture sim of ttc_eric_tb is

  constant verbose : integer := 1;

--  constant half_clock_period : time := 1.5625 ns;
  constant half_clock_period : time := 1.5 ns;
  constant clock_period      : time := 2 * half_clock_period;
  signal stop_the_clock      : boolean;  -- needed to stop XIlinx simulator

  signal sys_clk    : std_logic                     := '0';
  signal sys_rst    : std_logic                     := '0';
  signal sys_bx_stb : std_logic                     := '0';
  signal ttc_evn    : std_logic_vector(11 downto 0) := (others => '0');
  signal ttc_bcid   : std_logic_vector(11 downto 0) := (others => '0');
  signal ttc_orid   : std_logic_vector(11 downto 0) := (others => '0');

  -- simulator time
  signal sim_tick : integer := 0;

  signal ocr_req : std_logic;
  signal ecr_req : std_logic;
  signal l0a_req : std_logic;
  signal sys_bcr : std_logic;
  signal sys_ecr : std_logic;
  signal sys_ocr : std_logic;
  signal l0a     : std_logic;

  signal full_bunch : std_logic;

begin  -- architecture sim

  stimulus : process(sys_clk) is

  begin

    ttc_evn <= std_logic_vector(to_unsigned(1, ttc_evn'length));
    ocr_req <= '0';
    ecr_req <= '0';
--    l0a_req <= '0';

    if rising_edge(sys_clk) then        -- rising clock edge

      -- assert reset for 4 clocks
      if sim_tick < 4 then
        sys_rst <= '1';
      else
        sys_rst <= '0';

        if (sim_tick mod 100) = 0 then
          l0a_req <= '1';
        else
          l0a_req <= '0';
        end if;

      end if;

    end if;  -- rising_edge( sys_clk);
    --------------------------------------------------
    -- end of main loop
    --------------------------------------------------

  end process;

--------------------------------------------------
-- generate clock and basic TTC timing
-- update sim time:  sim_orn, sim_bcn, sim_pha
--------------------------------------------------

  -- first just generate the clock; all else runs off this clock
  g_clk : process
  begin
    while true loop
      sys_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
  end process;

  -- synchronous process to update other things
  process (sys_clk) is
  begin  -- process
    if rising_edge(sys_clk) then        -- rising clock edge

      sim_tick <= sim_tick + 1;

    end if;
  end process;

------------------------------------------------------------
-- DUT -----------------------------------------------------  
------------------------------------------------------------

  ttc_eric_1 : entity work.ttc_eric
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      ocr_req    => ocr_req,
      ecr_req    => ecr_req,
      l0a_req    => l0a_req,
      sys_bx_stb => sys_bx_stb,
      full_bunch => full_bunch,
      sys_bcn    => ttc_bcid,
      sys_orn    => ttc_orid,
      sys_bcr    => sys_bcr,
      sys_ecr    => sys_ecr,
      sys_ocr    => sys_ocr,
      l0a        => l0a);

end architecture sim;
