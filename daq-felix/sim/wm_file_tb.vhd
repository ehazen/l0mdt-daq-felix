--
-- simple testbench for WM
--
-- reads triggers and hits from a file "input_vectors.txt"
--
-- input vector format
--   flag s_orn s_bcn s_pha e_bcn seq_no
--
--   flag is "T" or "H" for trigger or hit
--   next 3 are timestamp to apply vector
--     s_orn - orbit number, starting with 0
--     s_bcn - bunch crossing, 0-3563
--     s_pha - pipeline clock phase, 0-7
--   e_bcn is the BcN of the "event" (trigger or hit)
--   seq_no is a unique sequence number for the item
--
-- E.Hazen
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;


entity wm_file_tb is
end entity wm_file_tb;

architecture sim of wm_file_tb is

  constant verbose : integer := 1;

-- configuration of window match entities
  constant NUM_WM          : integer := 4;
  constant WM_SEL_WID      : integer := 2;
  constant HIT_WIDTH       : integer := 41;  -- input HIT data width in bits
  constant BX_BIT_OFFSET   : integer := 0;   -- offset of BX in input hits
  constant TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
  constant TRIG_WIN_WIDTH  : integer := 40;  -- width of trigger window in BX
  constant TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
  constant TRIG_TIMEOUT    : integer := 40;  -- max additional time to wait for FIFO to empty

--  constant half_clock_period : time := 1.5625 ns;
  constant half_clock_period : time := 1.5 ns;
  constant clock_period      : time := 2 * half_clock_period;
  signal stop_the_clock      : boolean;  -- needed to stop XIlinx simulator

  signal sys_clk    : std_logic                              := '0';
  signal sys_rst    : std_logic                              := '0';
  signal sys_bx_stb : std_logic                              := '0';
  signal ttc_bc0    : std_logic                              := '0';
  signal ttc_l1a    : std_logic                              := '0';
  signal ttc_evn    : std_logic_vector(11 downto 0)          := (others => '0');
  signal ttc_bcid   : std_logic_vector(11 downto 0)          := (others => '0');
  signal ttc_orid   : std_logic_vector(11 downto 0)          := (others => '0');
  signal s_hit      : std_logic_vector(HIT_WIDTH-1 downto 0) := (others => '0');
  signal s_hit_dv   : std_logic                              := '0';

  file file_VECTORS : text;
  file file_RESULTS : text;

  -- simulator time
  signal sim_tick : integer                 := 0;
  signal sim_bcn  : integer range 0 to 4095 := 0;
  signal sim_orn  : integer range 0 to 4095 := 0;
  signal sim_pha  : integer range 0 to 7    := 7;

  signal s_matching : std_logic;        -- accepting hits
  signal s_busy     : std_logic;        -- this window matcher is busy
  signal s_count    : std_logic_vector(8 downto 0);  -- number of words in FIFO
  signal s_empty    : std_logic;        -- FIFO is empty
  signal s_full     : std_logic;        -- FIFO is full
  signal s_re       : std_logic;        -- FIFO read enable
  signal d_re : std_logic;              -- delayed read enable
  signal s_start    : std_logic;
  signal s_data     : std_logic_vector(HIT_WIDTH-1 downto 0);

begin  -- architecture sim

  stimulus : process(sys_clk) is

    -- variables used for file I/O
    variable row                        : line;
    variable buff                       : line;
    variable outb                       : line;
    variable flag                       : character;
    variable s_bcn, s_orn, e_bcn, s_pha : integer;
    variable seq_no                     : integer;
    variable fst                        : file_open_status;

  begin

    ttc_evn <= std_logic_vector(to_unsigned(1, ttc_evn'length));

    if rising_edge(sys_clk) then        -- rising clock edge

      s_hit_dv <= '0';
      d_re <= s_re;

      -- assert reset for 4 clocks
      if sim_tick < 4 then
        sys_rst <= '1';
        s_re <= '0';
      else
        sys_rst <= '0';
        s_re <= '1';
      end if;

      -- open files on tick 5
      if sim_tick = 5 then

        file_open(fst, file_VECTORS, "input_vectors.txt", read_mode);
        file_open(fst, file_RESULTS, "output_results.txt", write_mode);

        write(buff, string'("files open"));
        writeline(output, buff);

        -- read the first vector so it's ready
        while true loop
          readline(file_VECTORS, row);
          read(row, flag);

          if flag /= '#' then           --ignore comments
            read(row, s_orn);
            read(row, s_bcn);
            read(row, s_pha);
            read(row, e_bcn);
            read(row, seq_no);
            exit;
          end if;

        end loop;

        -- debug output
        if verbose > 0 then
          write(buff, string'("First VEC (orn, bcn, pha) "));
          write(buff, s_orn);
          write(buff, ' ');
          write(buff, s_bcn);
          write(buff, ' ');
          write(buff, s_pha);
          write(buff, ' ');
          write(buff, seq_no);
          writeline(output, buff);
        end if;

      end if;

      --------------------------------------------------
      -- main loop... stop when vector file is empty
      -- (or killed by simulation time limit)
      --------------------------------------------------

      if verbose > 1 then
        write(buff, string'("TICK ("));
        write(buff, now);
        write(buff, ' ');
        write(buff, sim_orn);
        write(buff, ' ');
        write(buff, sim_bcn);
        write(buff, ' ');
        write(buff, sim_pha);
        write(buff, ' ');
        write(buff, seq_no);
        write(buff, ')');
        writeline(output, buff);
      end if;

      -- write WM output
      if d_re = '1'  and s_empty = '0' then
        hwrite( outb, s_empty & "00" & s_data);
        writeline( file_results, outb);
      end if;

      if sim_pha = 0 then
        ttc_l1a <= '0';
      end if;

      -- is the next vector ready to be asserted?
      if (sim_orn = s_orn) and (sim_bcn = s_bcn) and (sim_pha = s_pha) then

        -- yes, check for (T)rigger or (H)it record (ignore others)
        if flag = 'T' then
          -- process trigger
          ttc_bcid <= std_logic_vector(to_unsigned(e_bcn, ttc_bcid'length));
          ttc_l1a  <= '1';
          if verbose > 0 then
            write(buff, string'("Trigger at "));
            write(buff, now);
            writeline(output, buff);
          end if;
        elsif flag = 'H' then
          -- process hit
          s_hit_dv                                     <= '1';
          s_hit(BX_BIT_OFFSET+11 downto BX_BIT_OFFSET) <= std_logic_vector(to_unsigned(e_bcn, 12));
          -- put some extra data in the hit for simulation
          s_hit(15 downto 12)                          <= std_logic_vector(to_unsigned(s_pha, 4));
          -- Put some sort of unique ID in upper bits for validation
          s_hit(HIT_WIDTH-1 downto 16)                 <= std_logic_vector(to_unsigned(seq_no, HIT_WIDTH-16));
          if verbose > 0 then
            write(buff, string'("hit at "));
            write(buff, now);
            writeline(output, buff);
          end if;
--        elsif flag = 'R' then
--          -- process read request
--          s_re <= '1';
        end if;

        -- read, skipping comments to next vector
        while not endfile(file_VECTORS) loop
          readline(file_VECTORS, row);
          read(row, flag);

          if flag /= '#' then           --ignore comments
            read(row, s_orn);
            read(row, s_bcn);
            read(row, s_pha);
            read(row, e_bcn);
            read(row, seq_no);
            exit;
          end if;
        end loop;

        if verbose > 0 then
          -- debug output
          write(buff, string'("Next VEC (orn, bcn, pha) "));
          write(buff, s_orn);
          write(buff, ' ');
          write(buff, s_bcn);
          write(buff, ' ');
          write(buff, s_pha);
          write(buff, ' ');
          write(buff, seq_no);
          writeline(output, buff);
        end if;

      end if;

    end if;  -- rising_edge( sys_clk);
    --------------------------------------------------
    -- end of main loop
    --------------------------------------------------

  end process;

--------------------------------------------------
-- generate clock and basic TTC timing
-- update sim time:  sim_orn, sim_bcn, sim_pha
--------------------------------------------------

  -- first just generate the clock; all else runs off this clock
  g_clk : process
  begin
    while true loop
      sys_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
  end process;

  -- synchronous process to update other things
  process (sys_clk) is
  begin  -- process
    if rising_edge(sys_clk) then        -- rising clock edge

      sim_tick <= sim_tick + 1;

      if sim_pha = 7 then
        sim_pha    <= 0;
        sys_bx_stb <= '1';

        if sim_bcn = 3563 then
          sim_bcn <= 0;
        else
          sim_bcn <= sim_bcn + 1;
        end if;

      else
        sim_pha    <= sim_pha + 1;
        sys_bx_stb <= '0';
      end if;

    end if;
  end process;

------------------------------------------------------------
-- DUT -----------------------------------------------------  
------------------------------------------------------------

  wm_1 : entity work.wm
    generic map (
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT)
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      ttc_bc0    => ttc_bc0,
      start      => ttc_l1a,
      ttc_bcid   => ttc_bcid,
      ttc_orid   => ttc_orid,
      hit        => s_hit,
      hit_dv     => s_hit_dv,
      matching   => s_matching,
      busy       => s_busy,
      count      => s_count,
      empty      => s_empty,
      full       => s_full,
      re         => s_re,
      data       => s_data);

end architecture sim;
