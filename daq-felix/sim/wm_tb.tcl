#
# default top-level display for wm_tb
#
gtkwave::addSignalsFromList [list {top.wm_tb.s_clk}]
gtkwave::addSignalsFromList [list {top.wm_tb.s_bx_stb}]
gtkwave::addSignalsFromList [list {top.wm_tb.s_bc0}]
gtkwave::addSignalsFromList [list {top.wm_tb.s_start}]
gtkwave::addSignalsFromList [list {top.wm_tb.wm_1.state}]
gtkwave::addSignalsFromList [list {top.wm_tb.wm_1.my_bx}]
gtkwave::addSignalsFromList [list {top.wm_tb.wm_1.hit_dv}]
gtkwave::addSignalsFromList [list {top.wm_tb.wm_1.hit}]
gtkwave::addSignalsFromList [list {top.wm_tb.wm_1.fifo_we}]

gtkwave::/Time/Zoom/Zoom_Full
