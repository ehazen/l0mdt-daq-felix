--
-- simple testbench for window matcher
--
-- E.Hazen
--

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity wm_tb is
end entity wm_tb;

architecture sim of wm_tb is

-- "normal" settings in BX
  constant HIT_WIDTH       : integer := 41;  -- HIT data width
  constant BX_BIT_OFFSET   : integer := 0;   -- offset of BX in input hits
  constant TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
  constant TRIG_WIN_WIDTH  : integer := 40;  -- width of trigger window in BX
  constant TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
  constant TRIG_TIMEOUT    : integer := 200;  -- max time to wait for FIFO to empty

  component wm is
    generic (
      HIT_WIDTH       : integer;
      BX_BIT_OFFSET   : integer;
      TRIG_WIN_OFFSET : integer;
      TRIG_WIN_WIDTH  : integer;
      TRIG_MATCH_TIME : integer;
      TRIG_TIMEOUT    : integer);
    port (
      sys_clk    : in  std_logic;
      sys_rst    : in  std_logic;
      sys_bx_stb : in  std_logic;
      ttc_bc0    : in  std_logic;
      start      : in  std_logic;
      ttc_bcid   : in  std_logic_vector(11 downto 0);
      ttc_orid   : in  std_logic_vector(11 downto 0);
      hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);
      hit_dv     : in  std_logic;
      matching   : out std_logic;
      busy       : out std_logic;
      count      : out std_logic_vector(8 downto 0);
      empty      : out std_logic;
      full       : out std_logic;
      re         : in  std_logic;
      data       : out std_logic_vector(HIT_WIDTH-1 downto 0));
  end component wm;

  constant clock_period : time := 3.125 ns;
  signal stop_the_clock : boolean;

  -- simulator time
  signal sim_tick : integer                 := 0;
  signal sim_bcn  : integer range 0 to 4095 := 0;
  signal sim_orn  : integer range 0 to 4095 := 0;
  signal sim_pha  : integer range 0 to 7    := 7;

  signal s_clk      : std_logic;
  signal s_rst      : std_logic;
  signal s_bx_stb   : std_logic;
  signal s_bc0      : std_logic;
  signal s_start    : std_logic;
  signal s_bcid     : std_logic_vector(11 downto 0);
  signal s_orid     : std_logic_vector(11 downto 0);
  signal s_hit      : std_logic_vector(HIT_WIDTH-1 downto 0);
  signal s_hit_dv   : std_logic;
  signal s_busy     : std_logic;
  signal s_matching : std_logic;
  signal s_count    : std_logic_vector(8 downto 0);
  signal s_empty    : std_logic;
  signal s_full     : std_logic;
  signal s_re       : std_logic;
  signal s_data     : std_logic_vector(HIT_WIDTH-1 downto 0);

begin  -- architecture sim

  wm_1 : entity work.wm
    generic map (
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT)
    port map (
      sys_clk    => s_clk,
      sys_rst    => s_rst,
      sys_bx_stb => s_bx_stb,
      ttc_bc0    => s_bc0,
      start      => s_start,
      ttc_bcid   => s_bcid,
      ttc_orid   => s_orid,
      hit        => s_hit,
      hit_dv     => s_hit_dv,
      matching   => s_matching,
      busy       => s_busy,
      count      => s_count,
      empty      => s_empty,
      full       => s_full,
      re         => s_re,
      data       => s_data);

  stimulus : process
    variable buff : line;
  begin

    -- Put initialisation code here
    s_start  <= '0';
    s_bcid   <= (others => '0');
    s_orid   <= (others => '0');
    s_hit    <= (others => '0');
    s_hit_dv <= '0';
    s_re     <= '0';

    s_rst <= '1';
    wait for clock_period*4;
    s_rst <= '0';
    wait for clock_period*4;

    while true loop
      if (sim_bcn mod 10) = 0 then
        if sim_pha = 3 then
          s_start <= '1', '0' after clock_period;
        end if;
      end if;
      wait for clock_period;
    end loop;


    wait;

  end process;

  g_clk : process
  begin
    while not stop_the_clock loop
      s_clk    <= '1', '0' after clock_period / 2;
      sim_tick <= sim_tick + 1;
      if sim_pha = 7 then
        sim_pha <= 0;
      else
        sim_pha <= sim_pha + 1;
      end if;
      wait for clock_period;
    end loop;
  end process;

  g_bx : process
  begin
    while not stop_the_clock loop
      s_bx_stb <= '1', '0' after clock_period;
      if sim_bcn = 3563 then
        sim_bcn <= 0;
      else
        sim_bcn <= sim_bcn + 1;
      end if;
      wait for clock_period * 8;
    end loop;
  end process;

end architecture sim;
