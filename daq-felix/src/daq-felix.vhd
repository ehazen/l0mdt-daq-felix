library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity daq_felix is

  generic (
    INPUT_WIDTH  : integer := 42;       -- width of one MDT hit
    OUTPUT_WIDTH : integer := 230;      -- output width to FELIX
    TRIG_WIDTH   : integer := 12        -- trigger width (BCID)
    );

  port (
    clk320 : in std_logic;              -- pipeline clock
    rst    : in std_logic;              -- active high asynchronous reset

    -- input data from DAQ block (MDT hits)
    data_in          : in  std_logic_vector(INPUT_WIDTH-1 downto 0);  -- MDT hits
    data_in_valid    : in  std_logic;   -- input data valid
    data_in_start    : in  std_logic;   -- first word of event
    data_in_end      : in  std_logic;   -- last word of event
    data_in_ready    : out std_logic;   -- '0' to pause input
    -- input triggers from DAQ block (must precede hits)
    trig_in          : in  std_logic_vector(TRIG_WIDTH-1 downto 0);
    trig_valid       : in  std_logic;   -- new trigger
    data_in_overflow : in  std_logic;   -- upstream overflow
    -- output data to HAL
    data_out         : out std_logic_vector(OUTPUT_WIDTH-1 downto 0)
    data_out_ready   : out std_logic;   -- output word is ready
    data_out_full    : in  std_logic    -- downstream FIFO is full
    );

end entity daq_felix;


architecture arch of daq_felix is

  type state_type is ( sIDLE, sHEADER, sHITS, sNEXT, sTRAILER);
  signal state : state_type;

  signal s_output : std_logic_vector(OUTPUT_WIDTH-1 downto 0);

begin  -- architecture arch

  process (clk, rst) is
  begin  -- process
    if rst = '1' then                   -- asynchronous reset (active high)
      state <= sIDLE;
    elsif rising_edge(clk) then         -- rising clock edge

      case state is
        when sIDLE =>                   -- wait for trigger (only)
          if trig_valid = '1' then
            
          end if;
          ;
        when sHEADER => ;
        when sHITS => ;
        when sNEXT => ;
        when sTRAILER => ;
        when others => null;
      end case;

    end if;
  end process;

end architecture arch;
