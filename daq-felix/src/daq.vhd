--
-- daq.vhd : top-level L0MDT DAQ block
--
-- This block processes hits in one 320MHz pipeline, so in principle
-- 18 of these are needed per standard sector
--
-- Input data is assumed to be formatted as 41 bit words (though this
-- is a generic) with the 12-bit bunch crossing found at a specified
-- constant offset within the input data vector.
--
-- Each L1A opens a new trigger matching window.
-- BX for each hit is compared against a window starting
--   (TRIG_WIN_OFFSET) after the trigger time and ending
--   (TRIG_WIN_WIDTH) BX later
-- Matching hits are pushed to a 512-word FIFO
-- Matching continues until TRIG_MATCH_TIME after the trigger
--
-- Then, the window match block stays active until one of:
--   TRIG_TIMEOUT time expires -or-
--   The FIFO is empty (whichever occurs first)
--

library IEEE;
use IEEE.std_logic_1164.all;

entity daq is

  generic (
    HIT_WIDTH       : integer := 41;    -- HIT data width
    BX_BIT_OFFSET   : integer := 0;     -- offset of BX in input hits
    TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
    TRIG_WIN_WIDTH  : integer := 40;    -- width of trigger window in BX
    TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
    TRIG_TIMEOUT    : integer := 200;   -- max time to wait for FIFO to empty
    DAQ_WIDTH       : integer := 230;   -- DAQ output width
    DAQ_MULT        : integer := 4;     -- data words per DAQ output word
    SLOT_WIDTH      : integer := 48;    -- data slot in DAQ word (< HIT_WIDTH!)
    NUM_WM          : integer := 4;  -- No. window match.  MUST MATCH NEXT GENERIC
    WM_SEL_WID      : integer := 2      -- select width for above MUST MATCH
    );

  port (
    -- timing
    sys_clk    : in  std_logic;         -- pipeline clock
    sys_rst    : in  std_logic;         -- active high asynch reset
    sys_bx_stb : in  std_logic;         -- BX strobe
    ttc_bc0    : in  std_logic;         -- BC0 (not used?)
    -- trigger
    ttc_l1a    : in  std_logic;         -- Trigger strobe
    ttc_evn    : in  std_logic_vector(11 downto 0);  -- event number 12 bits only
    ttc_bcid   : in  std_logic_vector(11 downto 0);  -- trigger BCID
    ttc_orid   : in  std_logic_vector(11 downto 0);  -- trigger Orbit
    -- MDT hit data in, 320MHz
    hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);  -- MDT hit data
    hit_dv     : in  std_logic;         -- MDT hit data valid
    -- FELIX data out, 40MHz, synch'd to bx_stb
    felix      : out std_logic_vector(229 downto 0);  -- FELIX output data
    felix_dv   : out std_logic;         -- FELIX data valid
    felix_full : in  std_logic);        -- FELIX FIFO full

end entity daq;


architecture arch of daq is

  component format is
    generic (
      DAQ_WIDTH     : integer;
      DAQ_MULT      : integer;
      SLOT_WIDTH    : integer;
      NUM_WM        : integer;
      WM_SEL_WID    : integer;
      HIT_WIDTH     : integer;
      BX_BIT_OFFSET : integer);
    port (
      sys_clk     : in  std_logic;
      sys_rst     : in  std_logic;
      sys_bx_stb  : in  std_logic;
      ttc_bc0     : in  std_logic;
      ttc_l1a     : in  std_logic;
      ttc_evn     : in  std_logic_vector(11 downto 0);
      ttc_bcid    : in  std_logic_vector(11 downto 0);
      ttc_orid    : in  std_logic_vector(11 downto 0);
      wm_used     : in  std_logic_vector(WM_SEL_WID-1 downto 0);
      wm_sel      : out std_logic_vector(WM_SEL_WID-1 downto 0);
      wm_busy     : in  std_logic;
      wm_matching : in  std_logic;
      wm_full     : in  std_logic;
      wm_empty    : in  std_logic;
      wm_re       : out std_logic;
      wm_data     : in  std_logic_vector(HIT_WIDTH-1 downto 0);
      data_out    : out std_logic_vector(DAQ_WIDTH-1 downto 0);
      data_out_dv : out std_logic);
  end component format;

  component dispatch is
    generic (
      NUM_WM          : integer;
      WM_SEL_WID      : integer;
      HIT_WIDTH       : integer;
      BX_BIT_OFFSET   : integer;
      TRIG_WIN_OFFSET : integer;
      TRIG_WIN_WIDTH  : integer;
      TRIG_MATCH_TIME : integer;
      TRIG_TIMEOUT    : integer);
    port (
      sys_clk     : in  std_logic;
      sys_rst     : in  std_logic;
      sys_bx_stb  : in  std_logic;
      ttc_bc0     : in  std_logic;
      ttc_l1a     : in  std_logic;
      ttc_bcid    : in  std_logic_vector(11 downto 0);
      ttc_orid    : in  std_logic_vector(11 downto 0);
      all_busy    : out std_logic;
      wm_used     : out std_logic_vector(WM_SEL_WID-1 downto 0);
      hit         : in  std_logic_vector(HIT_WIDTH-1 downto 0);
      hit_dv      : in  std_logic;
      wm_sel      : in  std_logic_vector(WM_SEL_WID-1 downto 0);
      wm_busy     : out std_logic;
      wm_matching : out std_logic;
      wm_empty    : out std_logic;
      wm_full     : out std_logic;
      wm_re       : in  std_logic;
      wm_data     : out std_logic_vector(HIT_WIDTH-1 downto 0));
  end component dispatch;

  -- re-synchronize inputs to clock
  signal s_sys_bx_stb : std_logic;
  signal s_ttc_bc0    : std_logic;
  signal s_ttc_l1a    : std_logic;
  signal s_ttc_evn    : std_logic_vector(11 downto 0);
  signal s_ttc_bcid   : std_logic_vector(11 downto 0);
  signal s_ttc_orid   : std_logic_vector(11 downto 0);
  signal s_hit        : std_logic_vector(HIT_WIDTH-1 downto 0);
  signal s_hit_dv     : std_logic;

  -- interconnect signals
  signal s_wm_used     : std_logic_vector(WM_SEL_WID-1 downto 0);
  signal s_wm_sel      : std_logic_vector(WM_SEL_WID-1 downto 0);
  signal s_wm_busy     : std_logic;
  signal s_wm_matching : std_logic;
  signal s_wm_full     : std_logic;
  signal s_wm_empty    : std_logic;
  signal s_wm_re       : std_logic;
  signal s_wm_data     : std_logic_vector(HIT_WIDTH-1 downto 0);

  signal s_all_busy : std_logic;

begin  -- architecture arch

  -- resynch inputs to clock
  -- (not needed as of now, implements OK at 320MHz)

--  resync : process (sys_clk, sys_rst) is
--  begin  -- process resync
--    if rising_edge(sys_clk) then     -- rising clock edge
  s_sys_bx_stb <= sys_bx_stb;
  s_ttc_bc0    <= ttc_bc0;
  s_ttc_l1a    <= ttc_l1a;
  s_ttc_evn    <= ttc_evn;
  s_ttc_bcid   <= ttc_bcid;
  s_ttc_orid   <= ttc_orid;
  s_hit        <= hit;
  s_hit_dv     <= hit_dv;
--    end if;
--  end process resync;


  -- Window match dispatcher with trigger FIFO
  dispatch_1 : entity work.dispatch
    generic map (
      NUM_WM          => NUM_WM,
      WM_SEL_WID      => WM_SEL_WID,
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT)
    port map (
      -- timing/trigger, wired to DAQ inputs
      sys_clk     => sys_clk,
      sys_rst     => sys_rst,
      sys_bx_stb  => s_sys_bx_stb,
      ttc_bc0     => s_ttc_bc0,
      ttc_l1a     => s_ttc_l1a,
      ttc_bcid    => s_ttc_bcid,
      ttc_orid    => s_ttc_orid,
      --
      all_busy    => s_all_busy,
      wm_used     => s_wm_used,
      -- data to WM array
      hit         => hit,
      hit_dv      => hit_dv,
      wm_sel      => s_wm_sel,
      wm_busy     => s_wm_busy,
      wm_matching => s_wm_matching,
      wm_empty    => s_wm_empty,
      wm_full     => s_wm_full,
      wm_re       => s_wm_re,
      wm_data     => s_wm_data);


  -- readout formatter
  format_1 : entity work.format
    generic map (
      DAQ_WIDTH     => DAQ_WIDTH,
      DAQ_MULT      => DAQ_MULT,
      SLOT_WIDTH    => SLOT_WIDTH,
      NUM_WM        => NUM_WM,
      WM_SEL_WID    => WM_SEL_WID,
      HIT_WIDTH     => HIT_WIDTH,
      BX_BIT_OFFSET => BX_BIT_OFFSET)
    port map (
      -- timing/trigger, wired to DAQ inputs
      sys_clk     => sys_clk,
      sys_rst     => sys_rst,
      sys_bx_stb  => s_sys_bx_stb,
      ttc_bc0     => s_ttc_bc0,
      ttc_l1a     => s_ttc_l1a,
      ttc_evn     => s_ttc_evn,
      ttc_bcid    => s_ttc_bcid,
      ttc_orid    => s_ttc_orid,
      -- internal connections
      wm_used     => s_wm_used,
      wm_sel      => s_wm_sel,
      wm_busy     => s_wm_busy,
      wm_matching => s_wm_matching,
      wm_full     => s_wm_full,
      wm_empty    => s_wm_empty,
      wm_re       => s_wm_re,
      wm_data     => s_wm_data,
      -- final ouput, wired to ports
      data_out    => felix,
      data_out_dv => felix_dv);

end architecture arch;
