------------------------------------------------------------
-- daq_unit.vhd - top-level self-contained DAQ for testing
------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity daq_unit is

  generic (
    -- TTC parameters
    ORBIT_LENGTH      : integer := 3564;  -- LHC orbit length
    PIPE_CLK_MULTIPLE : integer := 8;   -- pipeline clk mult of 40MHz
    -- DAQ parameters
    HIT_WIDTH         : integer := 41;  -- HIT data width
    BX_BIT_OFFSET     : integer := 0;   -- offset of BX in input hits
    TRIG_WIN_OFFSET   : integer := 10;  -- offset to start of trigger match (BX)
    TRIG_WIN_WIDTH    : integer := 40;  -- width of trigger window in BX
    TRIG_MATCH_TIME   : integer := 80;  -- min time (BX) to wait for matching hits
    TRIG_TIMEOUT      : integer := 200;   -- max time to wait for FIFO to empty
    DAQ_WIDTH         : integer := 230;   -- DAQ output width
    DAQ_MULT          : integer := 4;   -- data words per DAQ output word
    SLOT_WIDTH        : integer := 48;  -- data slot in DAQ word (< HIT_WIDTH!)
    NUM_WM            : integer := 4;  -- No. window match.  MUST MATCH NEXT GENERIC
    WM_SEL_WID        : integer := 2    -- select width for above MUST MATCH
    );

  port(
    sys_clk : in std_logic;             -- pipeline clock
    sys_rst : in std_logic;             -- async reset active high

    trig_rate : in std_logic_vector(31 downto 0);  -- trigger rate threshold
    hit_rate  : in std_logic_vector(31 downto 0);  -- hit rate threshold

    ocr_req : in std_logic;             -- orbit count reset strobe
    ecr_req : in std_logic;             -- event count reset strobe

    felix      : out std_logic_vector(229 downto 0);  -- FELIX output data
    felix_dv   : out std_logic;                       -- FELIX data valid
    felix_full : in  std_logic;
    -- for rate_monitoring
    trig_valid : out std_logic;
    hit_dv     : out std_logic
    );                                                -- FELIX FIFO full

end entity daq_unit;

architecture arch of daq_unit is

  signal sys_bx_stb : std_logic;
  signal trig_req   : std_logic;
  signal trig_val   : std_logic;
  signal hit_dv_s   : std_logic;
  signal sys_bcr    : std_logic;
  signal sys_ecr    : std_logic;
  signal sys_ocr    : std_logic;

  signal sys_bcn : std_logic_vector(11 downto 0);
  signal sys_orn : std_logic_vector(11 downto 0);
  signal sys_evn : std_logic_vector(11 downto 0);

  signal hit : std_logic_vector(HIT_WIDTH-1 downto 0);  -- MDT hit data

  signal full_bunch : std_logic;


begin

  trig_valid <= trig_val;
  hit_dv     <= hit_dv_s;

  hit(BX_BIT_OFFSET+11 downto BX_BIT_OFFSET) <= sys_bcn;

  trig_gen_1 : entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      rate       => trig_rate,
      trig       => trig_req);

  trig_gen_2 : entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => '1',
      rate       => hit_rate,
      trig       => hit_dv_s);

  ttc_eric_1 : entity work.ttc_eric
    generic map (
      orbit_length      => ORBIT_LENGTH,
      pipe_clk_multiple => PIPE_CLK_MULTIPLE)
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      ocr_req    => ocr_req,
      ecr_req    => ecr_req,
      l0a_req    => trig_req,
      sys_orn    => sys_orn,
      sys_bcn    => sys_bcn,
      sys_evn    => sys_evn,
      full_bunch => full_bunch,
      sys_bx_stb => sys_bx_stb,
      sys_bcr    => sys_bcr,
      sys_ecr    => sys_ecr,
      sys_ocr    => sys_ocr,
      l0a        => trig_val);

  daq_1 : entity work.daq
    generic map (
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT,
      DAQ_WIDTH       => DAQ_WIDTH,
      DAQ_MULT        => DAQ_MULT,
      SLOT_WIDTH      => SLOT_WIDTH,
      NUM_WM          => NUM_WM,
      WM_SEL_WID      => WM_SEL_WID)
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      ttc_bc0    => sys_bcr,
      ttc_l1a    => trig_val,
      ttc_evn    => sys_evn,
      ttc_bcid   => sys_bcn,
      ttc_orid   => sys_orn,
      hit        => hit,
      hit_dv     => hit_dv_s,
      felix      => felix,
      felix_dv   => felix_dv,
      felix_full => felix_full);

end architecture arch;

