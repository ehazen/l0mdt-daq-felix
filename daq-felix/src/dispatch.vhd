--
-- dispatch.vhd : dispatch triggers to an array of wm entities
--
-- Manage a (potentially two-dimensional) array of wm
-- Assign wm in order.  If next WM is busy, assert all_busy
--
-- push triggers into a FIFO, along with the WM number and all_busy
--
-- This is quite simple-minded as of now.  next_wm is just a counter,
-- and fires start to the selected wm and advances (unless all busy).
--
-- The FIFO interfaces are just presented through a simple
-- asynchronous mux/demux
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.math_real.all;

entity dispatch is

  generic (
    -- number of WM (eventually 16, 24, 32)
    NUM_WM          : integer := 4;
    WM_SEL_WID      : integer := 2;  -- must supply this as log2(NUM_WM) for now
    -- these are passed on to the WM
    HIT_WIDTH       : integer := 41;
    BX_BIT_OFFSET   : integer := 0;
    TRIG_WIN_OFFSET : integer := 10;
    TRIG_WIN_WIDTH  : integer := 40;
    TRIG_MATCH_TIME : integer := 80;
    TRIG_TIMEOUT    : integer := 200);

  port (
    -- global signals
    sys_clk     : in  std_logic;        -- pipeline clock
    sys_rst     : in  std_logic;        -- active high asynchronous reset
    sys_bx_stb  : in  std_logic;        -- BX strobe (every ~8 clocks)
    ttc_bc0     : in  std_logic;        -- orbit strobe (every 3584 BX)
    -- trigger input
    ttc_l1a     : in  std_logic;        -- trigger (validated by sys_bx_stb)
    ttc_bcid    : in  std_logic_vector(11 downto 0);  -- trigger BCID
    ttc_orid    : in  std_logic_vector(11 downto 0);  -- trigger Orbit (used?)
    -- status/control
    all_busy    : out std_logic;        --  all WM busy on trigger
    wm_used     : out std_logic_vector(WM_SEL_WID-1 downto 0);  -- WM used
    -- hit input to wm array
    hit         : in  std_logic_vector(HIT_WIDTH-1 downto 0);
    hit_dv      : in  std_logic;
    -- readout interface (wm_sel asynchronously selects FIFO to read)
    wm_sel      : in  std_logic_vector(WM_SEL_WID-1 downto 0);  -- WM readout select
    -- below apply to selected WM block
    wm_busy     : out std_logic;
    wm_matching : out std_logic;
    wm_empty    : out std_logic;
    wm_full     : out std_logic;
    wm_re       : in  std_logic;
    wm_data     : out std_logic_vector(HIT_WIDTH-1 downto 0)
    );

end entity dispatch;


architecture arch of dispatch is

  component wm is
    generic (
      HIT_WIDTH       : integer;
      BX_BIT_OFFSET   : integer;
      TRIG_WIN_OFFSET : integer;
      TRIG_WIN_WIDTH  : integer;
      TRIG_MATCH_TIME : integer;
      TRIG_TIMEOUT    : integer);
    port (
      sys_clk    : in  std_logic;
      sys_rst    : in  std_logic;
      sys_bx_stb : in  std_logic;
      ttc_bc0    : in  std_logic;
      ttc_bcid   : in  std_logic_vector(11 downto 0);
      ttc_orid   : in  std_logic_vector(11 downto 0);
      hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);
      hit_dv     : in  std_logic;
      matching   : out std_logic;
      busy       : out std_logic;
      count      : out std_logic_vector(8 downto 0);
      empty      : out std_logic;
      full       : out std_logic;
      re         : in  std_logic;
      data       : out std_logic_vector(HIT_WIDTH-1 downto 0));
  end component wm;

  signal start : std_logic_vector(NUM_WM-1 downto 0);

  signal mux_busy     : std_logic_vector(NUM_WM-1 downto 0);
  signal mux_matching : std_logic_vector(NUM_WM-1 downto 0);
  signal mux_empty    : std_logic_vector(NUM_WM-1 downto 0);
  signal mux_full     : std_logic_vector(NUM_WM-1 downto 0);
  signal demux_re     : std_logic_vector(NUM_WM-1 downto 0);

  type data_mux_t is array (NUM_WM-1 downto 0) of std_logic_vector(HIT_WIDTH-1 downto 0);
  signal mux_data : data_mux_t;

  signal next_wm : integer range 0 to NUM_WM-1;

  signal test1 : std_logic;

begin  -- architecture arch

  -- FIFO interface multiplexer
  wm_matching <= mux_matching(to_integer(unsigned(wm_sel)));
  wm_empty    <= mux_empty(to_integer(unsigned(wm_sel)));
  wm_busy     <= mux_busy(to_integer(unsigned(wm_sel)));
  wm_full     <= mux_full(to_integer(unsigned(wm_sel)));
  wm_data     <= mux_data(to_integer(unsigned(wm_sel)));

  demux_re(to_integer(unsigned(wm_sel))) <= wm_re;

  wm_used <= std_logic_vector(to_unsigned(next_wm, wm_used'length));

  process (sys_clk, sys_rst) is
  begin  -- process
    if sys_rst = '1' then               -- asynchronous reset (active high)
      next_wm <= 0;
    elsif sys_clk'event and sys_clk = '1' then  -- rising clock edge

      start    <= (others => '0');
      all_busy <= '0';

      test1 <= ttc_l1a;

      if sys_bx_stb = '1' and ttc_l1a = '1' then
        if mux_busy(next_wm) = '1' then
          all_busy <= '1';
        else
          start(next_wm) <= '1';
          if next_wm = NUM_WM-1 then
            next_wm <= 0;
          else
            next_wm <= next_wm + 1;
          end if;
        end if;
      end if;

    end if;
  end process;

  wms : for i in 0 to NUM_WM-1 generate

    wm_1 : entity work.wm
      generic map (
        HIT_WIDTH       => HIT_WIDTH,
        BX_BIT_OFFSET   => BX_BIT_OFFSET,
        TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
        TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
        TRIG_MATCH_TIME => TRIG_MATCH_TIME,
        TRIG_TIMEOUT    => TRIG_TIMEOUT)
      port map (
        sys_clk    => sys_clk,
        sys_rst    => sys_rst,
        sys_bx_stb => sys_bx_stb,
        ttc_bc0    => ttc_bc0,
        start      => start(i),
        ttc_bcid   => ttc_bcid,
        ttc_orid   => ttc_orid,
        hit        => hit,
        hit_dv     => hit_dv,
        matching   => mux_matching(i),
        busy       => mux_busy(i),
        count      => open,
        empty      => mux_empty(i),
        full       => mux_full(i),
        re         => demux_re(i),
        data       => mux_data(i));

  end generate wms;

end architecture arch;
