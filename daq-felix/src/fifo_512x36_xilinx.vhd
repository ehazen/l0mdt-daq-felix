--
-- wrapper for Xilinx 512x36 2 clock FIFO IP core
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo_512x36 is

  port (
    rst         : in  std_logic;
    wr_clk      : in  std_logic;
    rd_clk      : in  std_logic;
    din         : in  std_logic_vector(35 downto 0);
    wr_en       : in  std_logic;
    rd_en       : in  std_logic;
    dout        : out std_logic_vector(35 downto 0);
    full        : out std_logic;
    empty       : out std_logic;
    wr_rst_busy : out std_logic;
    rd_rst_busy : out std_logic);

end entity fifo_512x36;


architecture arch of fifo_512x36 is

  component vu_fifo_512x72 is
    port (
      clk         : IN  STD_LOGIC;
      srst        : IN  STD_LOGIC;
      din         : IN  STD_LOGIC_VECTOR(71 DOWNTO 0);
      wr_en       : IN  STD_LOGIC;
      rd_en       : IN  STD_LOGIC;
      dout        : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      full        : OUT STD_LOGIC;
      empty       : OUT STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC);
  end component vu_fifo_512x72;

begin  -- architecture arch

  fifo_512x36_2clk_1: fifo_512x36_2clk
    port map (
      rst         => rst,
      clk      => wr_clk,
      din         => din,
      wr_en       => wr_en,
      rd_en       => rd_en,
      dout        => dout,
      full        => full,
      empty       => empty,
      wr_rst_busy => wr_rst_busy,
      rd_rst_busy => rd_rst_busy);

end architecture arch;
