library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library extras;
use extras.fifos.all;

entity fifo_512xN is

  generic (
    WIDTH : integer := 72);

  port (
    rst   : in  std_logic;
    clk   : in  std_logic;
    din   : in  std_logic_vector(WIDTH-1 downto 0);
    wr_en : in  std_logic;
    rd_en : in  std_logic;
    dout  : out std_logic_vector(WIDTH-1 downto 0);
    full  : out std_logic;
    empty : out std_logic);

end entity fifo_512xN;

architecture arch of fifo_512xN is

  component fifo is
    generic (
      RESET_ACTIVE_LEVEL : std_ulogic;
      MEM_SIZE           : positive;
      SYNC_READ          : boolean);
    port (
      Wr_clock            : in  std_ulogic;
      Wr_reset            : in  std_ulogic;
      We                  : in  std_ulogic;
      Wr_data             : in  std_ulogic_vector;
      Rd_clock            : in  std_ulogic;
      Rd_reset            : in  std_ulogic;
      Re                  : in  std_ulogic;
      Rd_data             : out std_ulogic_vector;
      Empty               : out std_ulogic;
      Full                : out std_ulogic;
      Almost_empty_thresh : in  natural range 0 to MEM_SIZE-1 := 1;
      Almost_full_thresh  : in  natural range 0 to MEM_SIZE-1 := 1;
      Almost_empty        : out std_ulogic;
      Almost_full         : out std_ulogic);
  end component fifo;

  signal s_din, s_dout : std_ulogic_vector( WIDTH-1 downto 0);

begin  -- architecture arch

  dout <= std_logic_vector(s_dout);
  s_din <= std_ulogic_vector( din);

  fifo_1: entity extras.fifo
    generic map (
      RESET_ACTIVE_LEVEL => '1',
      MEM_SIZE           => 512,
      SYNC_READ          => true)
    port map (
      Wr_clock            => clk,
      Wr_reset            => rst,
      We                  => wr_en,
      Wr_data             => s_din,
      Rd_clock            => clk,
      Rd_reset            => rst,
      Re                  => rd_en,
      Rd_data             => s_dout,
      Empty               => empty,
      Full                => full,
      Almost_empty_thresh => 10,
      Almost_full_thresh  => 500,
      Almost_empty        => open,
      Almost_full         => open);

end architecture arch;
