--
-- wrapper for Xilinx 512x72 1 clock FIFO IP core
-- can be sized by generic to less than 72 bits
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo_512xN is

  generic (
    WIDTH : integer := 72);

  port (
    rst         : in  std_logic;
    clk      : in  std_logic;
    din         : in  std_logic_vector(WIDTH-1 downto 0);
    wr_en       : in  std_logic;
    rd_en       : in  std_logic;
    dout        : out std_logic_vector(WIDTH-1 downto 0);
    full        : out std_logic;
    empty       : out std_logic);

end entity fifo_512xN;

architecture arch of fifo_512xN is

  component vu_fifo_512x72 is
    port (
      clk         : IN  STD_LOGIC;
      srst        : IN  STD_LOGIC;
      din         : IN  STD_LOGIC_VECTOR(71 DOWNTO 0);
      wr_en       : IN  STD_LOGIC;
      rd_en       : IN  STD_LOGIC;
      dout        : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      full        : OUT STD_LOGIC;
      empty       : OUT STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC);
  end component vu_fifo_512x72;

  signal v_din : std_logic_vector(71 downto 0);
  signal v_dout : std_logic_vector(71 downto 0);

begin  -- architecture arch

  dout <= v_dout( WIDTH-1 downto 0);
  v_din(WIDTH-1 downto 0) <= din;
  -- stop Vivado complaints
  v_din(71 downto WIDTH) <= (others => '0');

  vu_fifo_512x72_1: vu_fifo_512x72
    port map (
      clk         => clk,
      srst        => rst,
      din         => v_din,
      wr_en       => wr_en,
      rd_en       => rd_en,
      dout        => v_dout,
      full        => full,
      empty       => empty,
      wr_rst_busy => open,
      rd_rst_busy => open);

end architecture arch;
