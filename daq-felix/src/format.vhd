--
-- format.vhd - read FIFOS and format event fragments
--
-- Receive triggers and store in a (short) FIFO
-- Match triggers with WM FIFO outputs
-- Output data with header and (optionally) trailer
--
-- Currently includes the trigger FIFO
--
-- N.B. felix output strobe (data_out_dv) is aligned
-- only to the pipeline clock, not sys_bx_stb
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity format is

  generic (
    DAQ_WIDTH     : integer := 230;     -- DAQ output width
    DAQ_MULT      : integer := 4;       -- data words per DAQ output word
    SLOT_WIDTH    : integer := 48;      -- data slot in DAQ word (< HIT_WIDTH!)
    NUM_WM        : integer := 4;       -- number of window managers
    WM_SEL_WID    : integer := 2;       -- WM select width
    HIT_WIDTH     : integer := 41;      -- raw hit data width
    BX_BIT_OFFSET : integer := 0);      -- where to find BX in hit (not used?)

  port (
    -- timing
    sys_clk     : in  std_logic;        -- pipeline clock
    sys_rst     : in  std_logic;        -- active high asynch reset
    sys_bx_stb  : in  std_logic;        -- BX strobe
    ttc_bc0     : in  std_logic;        -- BC0 (not used?)
    -- trigger
    ttc_l1a     : in  std_logic;        -- Trigger strobe
    ttc_evn     : in  std_logic_vector(11 downto 0);  -- event number 12 bits only
    ttc_bcid    : in  std_logic_vector(11 downto 0);  -- trigger BCID
    ttc_orid    : in  std_logic_vector(11 downto 0);  -- trigger Orbit
    -- WM+FIFO interface
    wm_used     : in  std_logic_vector(WM_SEL_WID-1 downto 0);  -- WM used
    -- WM/FIFO selection for readout/status
    wm_sel      : out std_logic_vector(WM_SEL_WID-1 downto 0);  -- WM select
    wm_busy     : in  std_logic;        -- WM busy
    wm_matching : in  std_logic;        -- WM matching hits
    wm_full     : in  std_logic;        -- FIFO full
    wm_empty    : in  std_logic;        -- FIFO empty
    wm_re       : out std_logic;        -- FIFO read enable
    wm_data     : in  std_logic_vector(HIT_WIDTH-1 downto 0);   -- FIFO data
    -- output interface
    data_out    : out std_logic_vector(DAQ_WIDTH-1 downto 0);
    data_out_dv : out std_logic);       -- output data valid

end entity format;


architecture arch of format is

  component fifo_512xN is
    generic (
      WIDTH : integer);
    port (
      rst   : in  std_logic;
      clk   : in  std_logic;
      din   : in  std_logic_vector(WIDTH-1 downto 0);
      wr_en : in  std_logic;
      rd_en : in  std_logic;
      dout  : out std_logic_vector(WIDTH-1 downto 0);
      full  : out std_logic;
      empty : out std_logic);
  end component fifo_512xN;

  -- trigger FIFO interface signals
  -- trigger FIFO:  hold BCN, ORN, EVN and WM_SEL
  constant TRIG_FIFO_WIDTH  : integer := ttc_evn'length + ttc_bcid'length + ttc_orid'length + wm_used'length;
  constant TRIG_FIFO_WMNOFF : integer := ttc_evn'length + ttc_bcid'length + ttc_orid'length;

  signal trig_din   : std_logic_vector(TRIG_FIFO_WIDTH-1 downto 0) := (others => '0');
  signal trig_wr_en : std_logic := '0';
  signal trig_rd_en : std_logic := '0';
  signal trig_dout  : std_logic_vector(TRIG_FIFO_WIDTH-1 downto 0) := (others => '0');
  signal trig_full  : std_logic := '0';
  signal trig_empty : std_logic := '0';

  -- output data
  signal felix_header  : std_logic_vector(DAQ_WIDTH-1 downto 0) := (others => '0');
  signal felix_trailer : std_logic_vector(DAQ_WIDTH-1 downto 0) := (others => '0');
  signal felix_data    : std_logic_vector(DAQ_WIDTH-1 downto 0) := (others => '0');

  -- array of output data slots
  type slot_group_t is array (0 to DAQ_MULT-1) of std_logic_vector(SLOT_WIDTH-1 downto 0);
  signal slots : slot_group_t;

  type state_t is (sIDLE, sPREHEAD, sHEADER, sPREFILL, sFILL, sLAST, sTRAILER);
  signal state : state_t;

  signal slot_no : integer range 0 to DAQ_MULT-1;             --current slot in data word

  signal s_out_dv : std_logic;          -- output data valid 

  signal fifo_rd     : std_logic;       --WM fifo read request
  signal fifo_rd_out : std_logic;
  signal slot_fill   : std_logic;       --pipelined data slot fill

begin  -- architecture arch

  -- format a word to be saved in the trigger FIFO
  -- wm_used is the window manager number
  trig_din <= wm_used & ttc_orid & ttc_bcid & ttc_evn;

  -- extract from the trigger FIFO ouput data the window manager number
  wm_sel   <= trig_dout(TRIG_FIFO_WMNOFF+WM_SEL_WID-1 downto TRIG_FIFO_WMNOFF);

  -- generate asynchronous write to trigger fifo based on L1A and BX strobe
  trig_wr_en <= ttc_l1a and sys_bx_stb;  -- one trigger per BX

  
  -- wiring for FELIX output words (HEADER, DATA, TRAILER)
  
  ---------- HEADER (upper bits X"4") ----------
  -- upper bits X"4"
  felix_header(DAQ_WIDTH-1 downto DAQ_WIDTH-4)     <= X"4";
  felix_header(DAQ_WIDTH-5 downto TRIG_FIFO_WIDTH) <= (others => '0');
  -- just copy the trigger info for now
  felix_header(TRIG_FIFO_WIDTH-1 downto 0)         <= trig_dout;

  ---------- DATA (upper bits X"8") ----------
  felix_data(DAQ_WIDTH-1 downto DAQ_WIDTH-4) <= X"8";
  --
  fd1 : for i in 0 to DAQ_MULT-1 generate
    felix_data((i+1)*SLOT_WIDTH-1 downto i*SLOT_WIDTH) <= slots(i);
  end generate fd1;
  -- include the last slot number
  felix_data(SLOT_WIDTH*DAQ_MULT+3 downto SLOT_WIDTH*DAQ_MULT) <= std_logic_vector( to_unsigned( slot_no, 4));
  -- fill the rest with zeroes
  felix_data(DAQ_WIDTH-5 downto SLOT_WIDTH*DAQ_MULT+4) <= (others => '0');

  ---------- TRAILER (upper bits X"C") ----------
  felix_trailer(DAQ_WIDTH-1 downto DAQ_WIDTH-4)     <= X"C";
  -- fill middle with zeroes
  felix_trailer(DAQ_WIDTH-5 downto TRIG_FIFO_WIDTH) <= (others => '0');
  -- put the trigger in the lower bits
  felix_trailer(TRIG_FIFO_WIDTH-1 downto 0)         <= trig_dout;

  
---------- main synchronous process ----------
process (sys_clk, sys_rst) is

  begin  -- process

    if sys_rst = '1' then               -- asynchronous reset (active high)
      state   <= sIDLE;
      slot_no <= 0;

      for i in 0 to DAQ_MULT-1 loop
        slots(i) <= (others => '1');
      end loop;  -- i

    elsif rising_edge(sys_clk) then     -- rising clock edge

      trig_rd_en <= '0';
      s_out_dv   <= '0';
      fifo_rd    <= '0';

      slot_fill <= fifo_rd_out;         --pipeline the slot_fill signal
      data_out_dv <= s_out_dv;          --delay DV by one clock


      -- this is a bit ugly, and should be incorporated in the state machine.
      -- store data in slots(0..3) in data word and assert output DV when all
      -- slots filled
      if slot_fill = '1' then           --fill a slot in felix output
        slots(slot_no)(wm_data'length-1 downto 0)          <= wm_data;  -- latch it in output
        slots(slot_no)(SLOT_WIDTH-1 downto wm_data'length) <= (others => '0');
        if slot_no = DAQ_MULT-1 then    -- last slot done?
          s_out_dv <= '1';              -- send the data
          slot_no  <= 0;                -- reset the slot
        else
          slot_no <= slot_no + 1;       -- next slot
        end if;
      end if;

      --
      -- simple FSM for output formatting
      -- N.B. the states sPREHEAD, sPREFILL and sLAST may not
      -- really be needed but I haven't taken the time to optimise
      --
      case state is

        ---------- sIDLE: wait for trigger ----------
        when sIDLE =>                   -- waiting for trigger
          if trig_empty = '0' then      -- there's a trigger available
            trig_rd_en <= '1';          -- read it from the FIFO
            state      <= sPREHEAD;
          end if;

        ---------- sPREHEAD: delay to send header ----------
        when sPREHEAD =>                -- just delay one clock
          state <= sHEADER;

        ---------- sHEADER: send the header ----------
        when sHEADER =>                 -- got the trigger, format a header
          data_out <= felix_header;     -- send the header
          s_out_dv <= '1';              -- assert output strobe
          slot_no  <= 0;                -- start with slot 0

          if wm_empty = '0' then        -- FIFO data available
            state <= sPREFILL;
          else                          -- FIFO is empty
            if wm_matching = '0' then   -- done matching, no data
              state <= sTRAILER;        -- go direct to trailer
            else
              -- FIFO empty but waiting
              state <= sPREFILL;
            end if;
          end if;

        ---------- sPREFILL: delay before sending data words ----------
        when sPREFILL =>
          state <= sFILL;

        ---------- sFILL: fill and send data words ----------
        when sFILL =>                   -- filling data slots

          -- actual filling is handled by "this is ugly" code above
          -- all we do here is read the data FIFO
          data_out <= felix_data;

          if wm_empty = '0' then        -- FIFO data available
            fifo_rd <= '1';
          else
            if wm_matching = '0' then   -- done matching, no more data
              state <= sLAST;
              if slot_no /= 0 then      -- any "orphan" hits?
                s_out_dv <= '1';        -- yes, send them
              end if;
            end if;
          end if;

        ---------- sLAST:  wait to send last data word if needed ----------
        when sLAST =>
          state <= sTRAILER;

        ---------- sTRAILER: send FELIX trailer ----------
        when sTRAILER =>
          data_out <= felix_trailer;
          s_out_dv <= '1';
          state    <= sIDLE;

        when others =>
          state <= sIDLE;

      end case;

    end if;
  end process;

  -- asynchronous logic to generate one-clock FIFO read signal
  fifo_rd_out <= fifo_rd and not wm_empty;
  wm_re       <= fifo_rd_out;

  fifo_512xN_2: entity work.fifo_512xN
    generic map (
      WIDTH => TRIG_FIFO_WIDTH)
    port map (
      rst   => sys_rst,
      clk   => sys_clk,
      din   => trig_din,
      wr_en => trig_wr_en,
      rd_en => trig_rd_en,
      dout  => trig_dout,
      full  => trig_full,
      empty => trig_empty);

end architecture arch;
