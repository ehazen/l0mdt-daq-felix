-- hit_trig_gen : generate random hits and triggers
--
-- hit format for testing:
--   bits 11:0  -  Coarse time (BcN for hit)
--   bits 15:12 -  clock phase for hit
--   bits 27:16 -  EvN (12 bits)
--   bits 40:17 -  spare bits

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.Numeric_Std.all;
use ieee.math_real.all;

entity hit_trig_gen is

  generic (
    HIT_WIDTH     : integer := 41;      -- HIT data width
    BX_BIT_OFFSET : integer := 0;       -- offset of BX in input hits
    TRIG_RATE_KHZ : real := 1000.0;    -- trigger rate in kHz
    HIT_RATE_MHZ  : real := 82.0);     -- tube hit rate in MHz

  port (
    sys_clk    : in  std_logic;         -- pipeline clock
    sys_rst    : in  std_logic;         -- active hi asynch reset
    hit_data   : out std_logic_vector(HIT_WIDTH-1 downto 0);  -- simulated hit
    hit_dv     : out std_logic;                               -- hit valid
    l0a_out    : out std_logic;                        -- trigger out
    l0a_bcn    : out std_logic_vector(11 downto 0);    -- BcN assigned to trigger
    sys_bx_stb : in  std_logic;
    bunch_ena  : in  std_logic);

end entity hit_trig_gen;


architecture arch of hit_trig_gen is

    constant TRIG_THR : real := ((2.0**32.0)-1.0)*(TRIG_RATE_KHZ/40.0e3);
    signal trig_rate : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned( natural(TRIG_THR), 32));

    constant HIT_THR : real := ((2.0**32.0)-1.0)*(HIT_RATE_MHZ/320.0);
    signal hit_rate : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned( natural(HIT_THR), 32));

    signal hit_data : std_logic_vector(HIT_WIDTH-1 downto 0);
    signal hit_dv : std_logic;
    signal l0a_out : std_logic;
    signal l0a_bcn : std_logic_vector(11 downto 0);

begin  -- architecture arch

  -- random trigger generator
  trig_gen_1: entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      rate       => trig_rate,
      trig       => l0a_out);

  -- random hit generator
  trig_gen_2: entity work.trig_gen
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => '1',
      rate       => hit_rate,
      trig       => hit_dv);

end architecture arch;
