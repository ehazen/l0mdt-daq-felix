--
-- dummy top-level for synthesis test
--

library IEEE;
use IEEE.std_logic_1164.all;

entity top_dummy_vu3p is

  port (
    sys_clk    : in  std_logic;
    sys_rst    : in  std_logic;
    sys_bx_stb : in  std_logic;
    ttc_bc0    : in  std_logic;
    ttc_l1a    : in  std_logic;
    ttc_evn    : in  std_logic_vector(11 downto 0);
    ttc_bcid   : in  std_logic_vector(11 downto 0);
    ttc_orid   : in  std_logic_vector(11 downto 0);
    hit        : in  std_logic_vector(40 downto 0);
    hit_dv     : in  std_logic;
    felix      : out std_logic_vector(229 downto 0);
    felix_dv   : out std_logic;
    felix_full : in  std_logic);

end entity top_dummy_vu3p;

architecture arch of top_dummy_vu3p is

-- configuration of window match entities
    constant HIT_WIDTH       : integer := 41;    -- HIT data width
    constant BX_BIT_OFFSET   : integer := 0;     -- offset of BX in input hits
    constant TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
    constant TRIG_WIN_WIDTH  : integer := 40;    -- width of trigger window in BX
    constant TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
    constant TRIG_TIMEOUT    : integer := 200;   -- max time to wait for FIFO to empty
    constant DAQ_WIDTH       : integer := 230;   -- DAQ output width
    constant DAQ_MULT        : integer := 4;     -- data words per DAQ output word
    constant SLOT_WIDTH      : integer := 48;    -- data slot in DAQ word (< HIT_WIDTH!)
    constant NUM_WM          : integer := 32;     -- No. window match.  MUST MATCH NEXT
    constant WM_SEL_WID      : integer := 5;      -- select width for above MUST MATCH

  component daq is
    generic (
      HIT_WIDTH       : integer;
      BX_BIT_OFFSET   : integer;
      TRIG_WIN_OFFSET : integer;
      TRIG_WIN_WIDTH  : integer;
      TRIG_MATCH_TIME : integer;
      TRIG_TIMEOUT    : integer;
      DAQ_WIDTH       : integer;
      DAQ_MULT        : integer;
      SLOT_WIDTH      : integer;
      NUM_WM          : integer;
      WM_SEL_WID      : integer);
    port (
      sys_clk    : in  std_logic;
      sys_rst    : in  std_logic;
      sys_bx_stb : in  std_logic;
      ttc_bc0    : in  std_logic;
      ttc_l1a    : in  std_logic;
      ttc_evn    : in  std_logic_vector(11 downto 0);
      ttc_bcid   : in  std_logic_vector(11 downto 0);
      ttc_orid   : in  std_logic_vector(11 downto 0);
      hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);
      hit_dv     : in  std_logic;
      felix      : out std_logic_vector(229 downto 0);
      felix_dv   : out std_logic;
      felix_full : in  std_logic);
  end component daq;

begin  -- architecture arch


  daq_1 : entity work.daq
    generic map (
      HIT_WIDTH       => HIT_WIDTH,
      BX_BIT_OFFSET   => BX_BIT_OFFSET,
      TRIG_WIN_OFFSET => TRIG_WIN_OFFSET,
      TRIG_WIN_WIDTH  => TRIG_WIN_WIDTH,
      TRIG_MATCH_TIME => TRIG_MATCH_TIME,
      TRIG_TIMEOUT    => TRIG_TIMEOUT,
      DAQ_WIDTH       => DAQ_WIDTH,
      DAQ_MULT        => DAQ_MULT,
      SLOT_WIDTH      => SLOT_WIDTH,
      NUM_WM          => NUM_WM,
      WM_SEL_WID      => WM_SEL_WID)
    port map (
      sys_clk    => sys_clk,
      sys_rst    => sys_rst,
      sys_bx_stb => sys_bx_stb,
      ttc_bc0    => ttc_bc0,
      ttc_l1a    => ttc_l1a,
      ttc_evn    => ttc_evn,
      ttc_bcid   => ttc_bcid,
      ttc_orid   => ttc_orid,
      hit        => hit,
      hit_dv     => hit_dv,
      felix      => felix,
      felix_dv   => felix_dv,
      felix_full => felix_full);

end architecture arch;
