------------------------------------------------------------
-- Simple TTC signal simulator
-- Generate sys_bx_stb, sys_bcr on appropriate clocks
-- Generate L0A after next sys_bx_stb on l0a_req when full_bunch
------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
-- use work.l0mdt_types.all;

entity ttc_eric is

  generic (
    orbit_length      : integer := 3564;     -- LHC orbit length
    pipe_clk_multiple : integer := 8;        -- pipeline clk mult of 40MHz
    random_threshold  : real    := 1.0/40.0  -- 1/40 for 1MHz trigger rate
    );

  port (
    sys_clk    : in  std_logic;         -- pipeline clock
    sys_rst    : in  std_logic;         -- async reset active high
    -- control signals (input)
    ocr_req    : in  std_logic;         -- generate ocr
    ecr_req    : in  std_logic;         -- generate ecr
    l0a_req    : in  std_logic;         -- generate L0A 
    -- counts (output)
    sys_orn    : out std_logic_vector(11 downto 0);
    sys_bcn    : out std_logic_vector(11 downto 0);
    sys_evn    : out std_logic_vector(11 downto 0);
    -- this bunch has protons
    full_bunch : out std_logic;
    -- control signals (output)
    sys_bx_stb : out std_logic;         -- bunch clock enable
    sys_bcr    : out std_logic;         -- bunch count reset (BC0)
    sys_ecr    : out std_logic;         -- event count reset
    sys_ocr    : out std_logic;         -- orbit count reset
    l0a        : out std_logic          -- L0A trigger
    );

end entity ttc_eric;


architecture arch of ttc_eric is

  signal cur_clk : integer range 0 to pipe_clk_multiple-1 := 0;
  signal cur_bcn : unsigned(11 downto 0)                  := (others => '0');
  signal cur_orn : unsigned(11 downto 0)                  := (others => '0');
  signal cur_evn : unsigned(11 downto 0)                  := (others => '0');

  signal s_full_bunch : std_logic;

  signal s1_l0a_req    : std_logic;
  signal s2_l0a_req    : std_logic;

  signal end_orbit : std_logic;

  ------------ LHC bunch filling -------------------------------------------
  -- this is from 2005 so probably for HL-LHC it will be different!
  -- 3564 = {[(72b+8e) x 3 + 30e] x 2 + [(72b+8e) x 4 + 31e]} x 3 +
  --        {[(72b+8e) x 3 + 30e] x 3 + 81e}
  -- see e.g.
  -- https://indico.cern.ch/event/901165/contributions/3810911/attachments/2016328/3370199/007_filling_schemes_WP2_20200407.pdf

  signal bx_map : std_logic_vector(0 to 3563) :=

    -- repeate 1/3
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    -- (72b+8e)x4 + 31e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "0000000000000000000000000000000" &  -- 31e


    -- repeate 2/3
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    -- (72b+8e)x4 + 31e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "0000000000000000000000000000000" &  -- 31e


    -- repeate 3/3
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    -- (72b+8e)x4 + 31e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "0000000000000000000000000000000" &  -- 31e

    -- last line
    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    -- (72b+8e)x3 + 30e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "11111111111111111111111111111111111111111111111111111111111111111111111100000000" &  -- 72b+8e
    "000000000000000000000000000000" &  -- 30e

    "000000000000000000000000000000000000000000000000000000000000000000000000000000000";  -- 81e

begin  -- architecture arch

  sys_bcn <= std_logic_vector(cur_bcn);
  sys_orn <= std_logic_vector(cur_orn);
  sys_evn <= std_logic_vector(cur_evn);

  s_full_bunch <= bx_map(to_integer(unsigned(cur_bcn)));
  full_bunch   <= s_full_bunch;

  process (sys_clk, sys_rst) is
  begin  -- process
    if sys_rst = '1' then               -- asynchronous reset (active high)

      cur_bcn <= (others => '0');
      cur_orn <= (others => '0');
      cur_evn <= (others => '0');
      s1_l0a_req <= '0';
      s2_l0a_req <= '0';

    elsif sys_clk'event and sys_clk = '1' then  -- rising clock edge

      sys_bx_stb <= '0';
      sys_bcr    <= '0';

      s1_l0a_req <= l0a_req;

      if s1_l0a_req = '1' then
        s2_l0a_req <= '1';
      end if;

      if cur_clk = pipe_clk_multiple-1 then
        cur_clk <= 0;
      else
        cur_clk <= cur_clk + 1;
      end if;

      if cur_clk = 0 then               -- bunch-crossing items

        sys_bx_stb <= '1';              -- export BX strobe

        -- pipeline end-of-orbit warning to simplify OrN logic
        if cur_bcn = orbit_length-2 then
          end_orbit <= '1';
        else
          end_orbit <= '0';
        end if;

        if s2_l0a_req = '1' and s_full_bunch = '1' then
          l0a <= '1';
          cur_evn <= cur_evn + 1;
        else
          l0a <= '0';
        end if;

        s2_l0a_req <= '0';

        if ecr_req = '1' then
          cur_evn <= (others => '0');
        end if;

        if ocr_req = '1' then
          cur_orn <= (others => '0');
        elsif end_orbit = '1' then
          cur_orn <= cur_orn + 1;
        end if;

        -- handle orbit count
        if end_orbit = '1' then
          cur_bcn <= (others => '0');
          sys_bcr <= '1';
        else
          cur_bcn <= cur_bcn + 1;
        end if;

      end if;

    end if;
  end process;


end architecture arch;
