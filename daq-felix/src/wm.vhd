--
-- wm.vhd : window match block with integral FIFO
--
-- On 'start', set a trigger matching BX range and timeout counters
-- Process MDT hits and store those matching in a FIFO
-- Wait for either FIFO empty or timeout
-- Generics currently control window width, timeout etc
--   (these could easily be made programmable)
--
-- Operation controlled by a simple FSM:
--
-- start in IDLE state
--   on start, capture bcid and orid and set up registers with
--   window to match and timeout and match time counters
--   move to MATCH state
-- when match time expires
--   move to WAIT state
-- when either timeout expires or FIFO is empty
--  move back to IDLE state
--


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity wm is

  generic (
    HIT_WIDTH       : integer := 41;    -- HIT data width
    BX_BIT_OFFSET   : integer := 0;     -- offset of BX in input hits
    TRIG_WIN_OFFSET : integer := 10;  -- offset to start of trigger match (BX)
    TRIG_WIN_WIDTH  : integer := 40;    -- width of trigger window in BX
    TRIG_MATCH_TIME : integer := 80;  -- min time (BX) to wait for matching hits
    TRIG_TIMEOUT    : integer := 200);  -- max time to wait for FIFO to empty

  port (
    -- global signals
    sys_clk    : in  std_logic;         -- 320MHz system clock
    sys_rst    : in  std_logic;         -- asynchronous active high reset
    -- TTC signals, updated every 25nS
    sys_bx_stb : in  std_logic;  -- BX strobe every 8 clocks, 1 clock wide
    ttc_bc0    : in  std_logic;         -- BC0 orbit mark 
    -- signals provided to start activate this WM
    start      : in  std_logic;         -- activate this window matcher
    ttc_bcid   : in  std_logic_vector(11 downto 0);  -- trigger BCID
    ttc_orid   : in  std_logic_vector(11 downto 0);  -- trigger orbit (needed?)
    -- MDT hit data in, 320MHz
    hit        : in  std_logic_vector(HIT_WIDTH-1 downto 0);  -- MDT hit data
    hit_dv     : in  std_logic;         -- MDT hit data valid
    -- output signals
    matching   : out std_logic;         -- accepting hits
    busy       : out std_logic;         -- this window matcher is busy
    count      : out std_logic_vector(8 downto 0);   -- number of words in FIFO
    empty      : out std_logic;         -- FIFO is empty
    full       : out std_logic;         -- FIFO is full
    re         : in  std_logic;         -- FIFO read enable
    data       : out std_logic_vector(HIT_WIDTH-1 downto 0)
    );

end entity wm;


architecture arch of wm is

  component fifo_512xN is
    generic (
      WIDTH : integer);
    port (
      rst   : in  std_logic;
      clk   : in  std_logic;
      din   : in  std_logic_vector(WIDTH-1 downto 0);
      wr_en : in  std_logic;
      rd_en : in  std_logic;
      dout  : out std_logic_vector(WIDTH-1 downto 0);
      full  : out std_logic;
      empty : out std_logic);
  end component fifo_512xN;

  signal s_begin   : unsigned(11 downto 0);  -- start of match window
  signal s_end     : unsigned(11 downto 0);  -- end of match windows
  signal s_match   : unsigned(11 downto 0);  -- match count-down timer
  signal s_timeout : unsigned(11 downto 0);  -- timeout for FIFO empty
  signal s_bcid    : unsigned(11 downto 0);  -- trigger BCID
  signal s_orbit   : unsigned(11 downto 0);  -- trigger Orbit 

  signal fifo_we       : std_logic;     -- FIFO write enable
  signal fifo_re       : std_logic;     -- FIFO read enable
  signal do_fifo_reset : std_logic;     -- clear the FIFO (local)
  signal fifo_reset    : std_logic;     -- clear the FIFO (OR)
  signal fifo_empty    : std_logic;     -- FIFO is empty
  signal fifo_full     : std_logic;     -- FIFO is full

  type state_t is (sIDLE, sMATCH, sWAIT);
  signal state : state_t;

  signal d_hit : std_logic_vector(HIT_WIDTH-1 downto 0);  -- pipelined hit data

begin  -- architecture arch

  fifo_reset <= do_fifo_reset or sys_rst;
  empty      <= fifo_empty;
  full       <= fifo_full;
  fifo_re    <= re;

  process (sys_clk, sys_rst) is

    variable buff : line;

  begin  -- process
    if sys_rst = '1' then               -- asynchronous reset (active high)

      s_begin   <= (others => '0');
      s_end     <= (others => '0');
      s_match   <= (others => '0');
      s_timeout <= (others => '0');
      s_bcid    <= (others => '0');
      s_orbit   <= (others => '0');
      state     <= sIDLE;
      busy      <= '0';

    elsif sys_clk'event and sys_clk = '1' then  -- rising clock edge

      fifo_we       <= '0';
      do_fifo_reset <= '0';
      matching      <= '0';

      d_hit <= hit;                     --pipeline hit data

      case state is
        when sIDLE =>

          if start = '1' then
            state     <= sMATCH;
            busy      <= '1';
            s_begin   <= unsigned(ttc_bcid) + TRIG_WIN_OFFSET;
            s_end     <= unsigned(ttc_bcid) + TRIG_WIN_OFFSET+TRIG_WIN_WIDTH;
            s_bcid    <= unsigned(ttc_bcid);
            s_orbit   <= unsigned(ttc_orid);
            s_match   <= to_unsigned(TRIG_MATCH_TIME, s_match'length);
            s_timeout <= to_unsigned(TRIG_TIMEOUT, s_timeout'length);
          end if;

        when sMATCH =>
          matching <= '1';

          if s_match = 0 then
            state <= sWAIT;
          else
            if sys_bx_stb = '1' then
              s_match <= s_match - 1;
            end if;
          end if;

          if hit_dv = '1' then
            if (unsigned(hit(BX_BIT_OFFSET+11 downto BX_BIT_OFFSET))) >= s_begin and
              (unsigned(hit(BX_BIT_OFFSET+11 downto BX_BIT_OFFSET)) <= s_end) then
              fifo_we                                               <= '1';
            end if;
          end if;

        when sWAIT =>
          if s_timeout = 0 or fifo_empty = '1' then
            state         <= sIDLE;
            busy          <= '0';
            do_fifo_reset <= '1';       -- force FIFO reset on timeout
          else
            if sys_bx_stb = '1' then
              s_timeout <= s_timeout - 1;
            end if;
          end if;

        when others => null;
      end case;

    end if;
  end process;


  fifo_512xN_2 : fifo_512xN
    generic map (
      WIDTH => HIT_WIDTH)
    port map (
      rst   => fifo_reset,
      clk   => sys_clk,
      din   => d_hit,
      wr_en => fifo_we,
      rd_en => fifo_re,
      dout  => data,
      full  => fifo_full,
      empty => fifo_empty);

end architecture arch;
